import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'typeface-roboto';  
import "react-image-gallery/styles/css/image-gallery.css"; 

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import axios from 'axios';
// import Cookies from 'js-cookie';
// import setCSRF from './utils/setCSRF';

// setCSRF();
// axios.get('/api/v1/track')
//     .then(ress => {
//         axios.defaults.headers.common['csrf-token'] = ress.data;

//     });

        ReactDOM.render(
            <App />
            , document.getElementById('root'));




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
