import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import MainLayout from './components/layouts/MainLayout';
import Home from './components/pages/home'
import Products from './components/pages/products'
import ProductDetail from './components/pages/productDetail'
import ScrollToTop from './ScrollToTop'
import Carts from './components/pages/carts';
import PrivateRoute from './components/common/PrivateRoute';
import AuthRoute from './components/common/AuthRoute';
import Checkout from './components/pages/checkout';
import Login from './components/pages/auth/Login';
import Register from './components/pages/auth/Register';
import MyAccount from './components/pages/myAccount';
import Profile from './components/pages/profile';
import jwt_decode from 'jwt-decode';
import { setCurrentUser, clearCurrentUser} from './actions/authActions';
import { setCart, clearCartList} from './actions/cartActions';
import { setAddressCurrentUser, clearAddressUser} from './actions/myAccounts';
import {getContentHome} from './actions/uiActions';
import Address from './components/pages/address';
import Lookbook from './components/pages/lookbook';
import DetailLookbook from './components/pages/lookbook/detailLookbook';
import Order from './components/pages/order';
import Payment from './components/pages/payment';
import OrderDetail from './components/pages/order/detailOrder';
import PrivateRouteOrder from './components/common/PrivateRouteParams';
import Collection from './components/pages/collection';
import ReturnPolicy from './components/pages/returnPolicy';
import SizeGuide from './components/pages/size';
import PaymentGuide from './components/pages/paymentGuide';
import Help from './components/pages/help';
store.dispatch(getContentHome());
if (localStorage.hammerstout_a) {
  const decoded = jwt_decode(localStorage.hammerstout_a);
  store.dispatch(setCurrentUser(decoded.user));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    localStorage.removeItem("hammerstout_a");
    store.dispatch(clearCurrentUser());
  }
}

if (localStorage.hammerstout_address){
  const decoded = jwt_decode(localStorage.hammerstout_address);
  store.dispatch(setAddressCurrentUser(decoded.resultData));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    localStorage.removeItem("hammerstout_a");
    store.dispatch(clearAddressUser());
  }
}

if(localStorage.hammerstout_c){
  const decoded = jwt_decode(localStorage.hammerstout_c);
  store.dispatch(setCart(decoded.result));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    localStorage.removeItem("hammerstout_c");
    store.dispatch(clearCartList());
  }

}

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <Router >
          <Switch>
         
            <MainLayout>
              <ScrollToTop>
              <Switch>
             
              <Route path="/" exact render={(props)=>(
                <Home  {...props}/>
              )}  />
                  <Route path="/redirect" exact render={(props) => (
                    <Home  {...props} />
                  )} />
              <Route path="/shop" exact render={(props)=>(
                <Products  {...props}/>
              )} />
                <Route path="/carts" exact render={(props)=>(
                    <Carts {...props}/>
                )}/>
                  <Route path="/shop/:tag" exact render={(props)=>
                    (<Products key={props.match.params.tag} {...props} />)
                } />
                  <Route path="/shop/:tag/:category" exact render={(props) =>
                    (<Products key={props.match.params.category} {...props} />)
                  } />
                  <Route path="/shop/:tag/:category/:type" exact render={(props) =>
                    (<Products key={props.match.params.type} {...props} />)
                  } />

                  <Route path="/products/:category/:name" exact render={(props)=>
                (<ProductDetail  key={props.match.params.name} {...props}/>)
                  } />

                  <Route path="/lookbook" exact render={(props)=>
                  (<Lookbook {...props} />)}/>
                  <Route path="/collection" exact render={(props) =>
                    (<Collection {...props} />)} />
                  <Route path="/collection/:collection" exact render={(props) =>
                    (<Products key={props.match.params.collection}  {...props} />)} />
                  <Route path="/return-policy" exact render={(props) =>
                    (<ReturnPolicy  {...props} />)} />
                  <Route path="/size-guide" exact render={(props) =>
                    (<SizeGuide  {...props} />)} />
                  <Route path="/payment-guide" exact render={(props) =>
                    (<PaymentGuide  {...props} />)} />
                  <Route path="/help" exact render={(props)=>(
                    <Help {...props}/>
                  )} />
                  
                  <PrivateRoute path="/checkout" exact component={Checkout}
                  />


                  <PrivateRouteOrder path="/checkout/:token_order" exact component={Payment}
                  />
                  <Route path="/lookbook/detail/:slug" exact render={(props)=>
                    (<DetailLookbook key={props.match.params.slug} {...props} />)} />

                  <AuthRoute path="/sign-in" exact component={Login}/>
                  <AuthRoute path="/sign-up" exact component={Register}/>

                  <PrivateRoute path="/my-account" exact component={MyAccount}/>
                  <PrivateRoute path="/my-account/profile" exact component={Profile} />
                  <PrivateRoute path="/my-account/address" exact component={Address} />
                  <PrivateRoute path="/my-account/orders" exact component={Order}/>

                  <PrivateRouteOrder path="/my-account/orders/detail/:token_order" exact component={OrderDetail}/>
              </Switch>
               </ScrollToTop>
            </MainLayout>
           
          </Switch>
         
   
        </Router>
      </Provider>
    
    );
  }
}

export default App;
