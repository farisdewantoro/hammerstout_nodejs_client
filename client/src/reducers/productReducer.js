import { GET_PRODUCT_HIGHLIGHT, LOADING_PRODUCT, GET_PRODUCT_WITH_QUERY, GET_PRODUCT, GET_PRODUCT_DETAIL} from '../actions/types';
const initialState = {
    loading: false,
    product: [],
    productDetail:{},
    todayHighlight:[],
    pagination:[],
    status:null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case LOADING_PRODUCT:
            return {
                ...state,
                loading: true
            }
        case GET_PRODUCT_DETAIL:
            return{
                ...state,
                loading:false,
                productDetail:action.payload
            }
        case GET_PRODUCT:
            return{
                ...state,
                loading:false,
                product: action.payload.product,
                pagination:action.payload.pagination
            }
        case GET_PRODUCT_WITH_QUERY:
            return{
                ...state,
                loading:false,
                product:action.payload
            }
      
        case GET_PRODUCT_HIGHLIGHT:
            return{
                ...state,
                loading:false,
                todayHighlight:action.payload
            }
        default:
            return state;
    }
}