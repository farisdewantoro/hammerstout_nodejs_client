import axios from 'axios';
import Cookies from 'js-cookie';
const setCSRF = () => {
    let csrf = Cookies.get('hammerstout_t');
    console.log(csrf);
    if (csrf) {
       
        // Apply to every request
        axios.defaults.headers.common['csrf-token'] = csrf;
    }
    else {
        // Delete Auth Header
        delete axios.defaults.headers.common['csrf-token'];
    }
}


export default setCSRF;