import React, { Component } from 'react'
import { compose } from 'redux'
import PropTypes from 'prop-types';
import { withStyles  } from '@material-ui/core/styles';
import ImageGallery from 'react-image-gallery';
import styles from './styles';

function imagesData(data, classes){
    let image =[];
    image= data.map((d,i)=>{
        return {
            original: d.link,
            thumbnail: d.link,
            originalAlt: d.alt,
            thumbnailAlt: d.alt,
            originalTitle: d.caption,
            thumbnailTitle: d.caption,
            public_id: d.public_id
        }
    })

    return image;
}
 class imageDetail extends Component {
  render() {
      const { classes, dataImage, width} = this.props;
  
      let position = "left";
       if(width === "xs"){
           position = "bottom";
       }

    return (
      <div className={classes.wrapperImageGallery}>
            <ImageGallery
                items={imagesData(dataImage,classes)}
                thumbnailPosition={position}
                showNav={false}
                
                showPlayButton={false}
                lazyLoad={true} 
                infinite={true}
                />
      </div>
    )
  }
}

imageDetail.propTypes = {
    classes: PropTypes.object.isRequired
}

export default compose(withStyles(styles))(imageDetail);