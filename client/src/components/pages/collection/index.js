import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import { getAllCollection } from '../../../actions/collectionActions';
import ReactPixel from 'react-facebook-pixel';
class Collection extends Component {
    componentDidMount() {
        this.props.getAllCollection();
        const options = {
            autoConfig: true, 	// set pixel's autoConfig
            debug: false, 		// enable logs
        };

        ReactPixel.init('365684027172803', options);
        ReactPixel.pageView();
    }
    render() {
        const { classes } = this.props;
        const { collection } = this.props.collections;
        return (
            <div className={classes.paddingRoot}>
                <Grid container >
                    <Grid item md={12}>
                        <Grid container justify="center">
                            <Grid item md={10}>
                                <Typography className={classes.titleCollection}>
                                    Collection
                                </Typography>
                                <Grid container direction="row" justify="center" spacing={16}>

                                    {collection.map((co, i) => {
                                        return (
                                            <Grid item md={12} key={i} >
                                                <Link to={`/collection/${co.slug}`}>
                                                    <img src={co.link} style={{ width: "100%" }} alt={co.alt} />
                                                </Link>
                                               
                                            </Grid>
                                        )
                                    })}

                                </Grid>

                            </Grid>
                        </Grid>

                    </Grid>

                </Grid>
            </div>
        )
    }
}

Collection.propTypes = {
    classes: PropTypes.object.isRequired,
    collections: PropTypes.object.isRequired,
    getAllCollection: PropTypes.func.isRequired
}

const mapStateToProps = state => ({ collections: state.collections });

export default compose(connect(mapStateToProps, { getAllCollection }), withStyles(styles, { name: "Collection" }))(Collection);
