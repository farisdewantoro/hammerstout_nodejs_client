import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes, { instanceOf } from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import Navigator from './Navigator';
import BillingDetail from './billing';
import { getProvince, findRegencies, findDistricts, findVillages } from '../../../actions/addressActions';
import OrderDetail from './order';
import {  getAddressLocation } from '../../../actions/myAccounts';
import { calculateCost} from '../../../actions/shippingActions';
import ShippingMethod from './shipping';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import { submitProceedToCheckout } from '../../../actions/checkoutActions';
import {withRouter} from 'react-router';
import Hidden from '@material-ui/core/Hidden';
import ReactPixel from 'react-facebook-pixel';
 class Checkout extends Component {
   state={
     user: {
       firstname: "",
       lastname: "",
     },
     email: "",
     user_information: {
       birthday: new Date(),
       phone_number: ""
     },
     provinces: [],
     regencies: [],
     districts: [],
     villages: [],
     user_address: {
       province_id: '',
       regency_id: '',
       district_id: '',
       village_id: '',
       postcode: '',
       address: ''
     },
     subTotal:0,
     shippingFee:0,
     shippingMethod:[],
     shippingSelected:{},
     total:0,

    
     
   }
   componentDidMount() {
     if (this.state.user_address.province_id !== '' && this.state.user_address.regency_id !== ''){
       this.props.calculateCost(this.state.user_address);
     }

     const options = {
       autoConfig: true, 	// set pixel's autoConfig
       debug: false, 		// enable logs
     };

     ReactPixel.init('365684027172803', options);
     ReactPixel.pageView();
    
     this
       .props
       .getProvince();
     this.props.getAddressLocation();
     if (Object.keys(this.props.auths.user_address).length > 0) {
       let user_a = this.props.auths.user_address;
       this.setState(prevState => ({
         user_address: {
           ...prevState.user_address,
           province_id: { value: user_a.province_id, label: user_a.province_name },
           regency_id: { value: user_a.regency_id, label: user_a.regency_name },
           district_id: { value: user_a.district_id, label: user_a.district_name },
           village_id: { value: user_a.village_id, label: user_a.village_name },
           postcode: user_a.postcode,
           address: user_a.address
         }
       }))
     }
     let stateUser = this.state.user;
     let stateUserInfo = this.state.user_information;
     let user = this.props.auths.user;
     if (Object.keys(user).length > 0) {
       Object.keys(stateUser).forEach(keys => {
         Object.keys(user).forEach(userKey => {
           this.setState(prevState => ({
             user: {
               ...prevState.user,
               [keys]: user[keys]
             }
           }))
         })
       });
       Object.keys(stateUserInfo).forEach(keys => {
         Object.keys(user).forEach(userKey => {

           this.setState(prevState => ({
             user_information: {
               ...prevState.user_information,
               [keys]: user[keys]
             }
           }))
         })
       });
       this.setState({
         email: user.email
       })

     }
   }
   shouldComponentUpdate(nextProps, nextState){
     let province = this.state.user_address.province_id;
     let regency = this.state.user_address.regency_id;
     let nextStateProvince = nextState.user_address.province_id;
     let nextStateRegency = nextState.user_address.regency_id;

     if ((province !== nextStateProvince) && regency !== '' && nextStateRegency !== '' 
      && nextStateProvince !== '' ){
        this.props.calculateCost(nextState.user_address);
        return false;
      }
     if ((province !== nextStateProvince) && nextStateRegency === '' && nextStateProvince === null ){
       this.props.calculateCost(nextState.user_address);
       return false;
       }
     if ((regency !== nextStateRegency) && province !== '' && nextStateProvince !== '' && nextStateRegency !== '' ){
       this.props.calculateCost(nextState.user_address);
       return false;
     }


  
     return true;

   }

   UNSAFE_componentWillReceiveProps(nextProps) {
     if (nextProps.address.provinces !== this.props.address.provinces) {
       this.setState({
         provinces: nextProps
           .address
           .provinces
           .map(p => {
             return { value: p.id, label: p.name }
           })
       })
     }
     if (nextProps.address.regencies !== this.props.address.regencies) {
       this.setState({
         regencies: nextProps
           .address
           .regencies
           .map(r => {
             return { value: r.id, label: r.name, province_id: r.province_id }
           })
       })
     }
     if (nextProps.address.districts !== this.props.address.districts) {
       this.setState({
         districts: nextProps
           .address
           .districts
           .map(d => {
             return { value: d.id, label: d.name, regency_id: d.regency_id }
           })
       })
     }
     if (nextProps.address.villages !== this.props.address.villages) {
       this.setState({
         villages: nextProps
           .address
           .villages
           .map(v => {
             return { value: v.id, label: v.name, district_id: v.district_id }
           })
       })
     }
     if (nextProps.auths.user_address !== this.props.auths.user_address) {
       let user_a = nextProps.auths.user_address;
       this.setState(prevState => ({
         user_address: {
           ...prevState.user_address,
           province_id: { value: user_a.province_id, label: user_a.province_name },
           regency_id: { value: user_a.regency_id, label: user_a.regency_name },
           district_id: { value: user_a.district_id, label: user_a.district_name },
           village_id: { value: user_a.village_id, label: user_a.village_name },
           postcode: user_a.postcode,
           address: user_a.address
         }
       }))
     }
     let user = nextProps.auths.user;
     let stateUser = this.state.user;
     let stateUserInfo = this.state.user_information;
     if (user !== this.props.auths.user) {
       Object.keys(stateUser).forEach(keys => {
         Object.keys(user).forEach(userKey => {
           this.setState(prevState => ({
             user: {
               ...prevState.user,
               [keys]: user[keys]
             }
           }))
         })
       });
       Object.keys(stateUserInfo).forEach(keys => {
         Object.keys(user).forEach(userKey => {
           this.setState(prevState => ({
             user_information: {
               ...prevState.user_information,
               [keys]: user[keys]
             }
           }))
         })
       });
       this.setState({
         email: user.email
       })
     }

     if(nextProps.shipping.data !== this.props.shipping.data && Object.keys(nextProps.shipping.data).length > 0){
       this.setState({
         shippingMethod: nextProps.shipping.data.results,
         shippingFee:0
       })
     }

     if (nextProps.shipping.data !== this.props.shipping.data && Object.keys(nextProps.shipping.data).length === 0) {
       this.setState({
         shippingMethod:[],
         shippingFee:0
       })
     }
  
   }


   handlerChangeProvince = value => {
     this.setState(prevState => ({
       user_address: {
         ...prevState.user_address,
         province_id: value,
         regency_id: '',
         district_id: '',
         village_id: '',

       }
     }));
     if (typeof value !== "undefined" && value !== null && (typeof value == "object" && Object.keys(value).length > 0)) {
       this.props.findRegencies(value);
     }
   }
   handleChangeRegency = value => {
     this.setState(prevState => ({
       user_address: {
         ...prevState.user_address,
         regency_id: value,
         district_id: '',
         village_id: ''
       }
     }));
     if (typeof value !== "undefined" && value !== null && (typeof value == "object" && Object.keys(value).length > 0)) {
       this.props.findDistricts(value);
     }
   }
   handlerChangeDistrict = value => {
     this.setState(prevState => ({
       user_address: {
         ...prevState.user_address,
         district_id: value,
         village_id: '',
       }
     }));
     if (typeof value !== "undefined" && value !== null && (typeof value == "object" && Object.keys(value).length > 0)) {
       this.props.findVillages(value);
     }
   }
   handlerChangeVillage = value => {
     this.setState(prevState => ({
       user_address: {
         ...prevState.user_address,
         village_id: value
       }
     }));
   }
   handlerChangeAddress = (e) => {
     let name = e.target.name;
     let value = e.target.value;
     this.setState(prevState => ({
       user_address: {
         ...prevState.user_address,
         [name]: value
       }
     }))
   }

   handlerChangeUser = (e) => {
     let name = e.target.name;
     let value = e.target.value;
     this.setState(prevState => ({
       user: {
         ...prevState.user,
         [name]: value
       }
     }))
   }
   handlerChangeUserInfo = (e) => {
     let name = e.target.name;
     let value = e.target.value;
     this.setState(prevState => ({
       user_information: {
         ...prevState.user_information,
         [name]: value
       }
     }))
   }
   handlerShippingMethod=(e)=>{
    this.setState({
       shippingSelected:e.target.value,
       shippingFee:JSON.parse(e.target.value).cost[0].value
     });
   }
   handlerSubmitOrder = () => {
     let shipping={};
     if (this.state.shippingSelected.length > 1){
       shipping = JSON.parse(this.state.shippingSelected)
     }


     let user ={
       firstname:this.state.user.firstname,
       lastname: this.state.user.lastname,
       email:this.state.email,
       phone_number:this.state.user_information.phone_number,
      province_id:this.state.user_address.province_id,
       regency_id: this.state.user_address.regency_id,
       district_id: this.state.user_address.district_id,
       village_id: this.state.user_address.village_id,
       postcode: this.state.user_address.postcode,
       address: this.state.user_address.address
     }
    let data={
      shippingSelected: shipping ,
      user:user,
      carts:this.props.carts.cartList,
      vouchers:this.props.vouchers.voucher
    }
     this.props.submitProceedToCheckout(data,this.props.history);
     const options = {
       autoConfig: true, 	// set pixel's autoConfig
       debug: false, 		// enable logs
     };

     let subTotal =0;
     let shippingCost = 0;
     if (this.props.carts.cartList instanceof Array && this.props.carts.cartList.length > 0){
       subTotal = this.props.carts.cartList.map(c => c.discount_value ? c.discount_value * c.quantity : c.regular_price * c.quantity)
         .reduce((a, b) => a + b)
     } 
     if(shipping){
       shippingCost = shipping.cost[0].value;
     }
     const dataFB = {
        value:subTotal,
        currency:'IDR',
       shipping: shippingCost
     }

     
     ReactPixel.init('365684027172803', options);
     ReactPixel.track('InitiateCheckout', dataFB);

   
   }
   
  
 
  render() {
    const { classes, carts,vouchers} = this.props;
    const { user, email, 
      user_information, 
      provinces, regencies, districts, villages, user_address, shippingMethod, shippingFee, total, shippingSelected
    } = this.state;
    const { loading } = this.props.shipping;

    return (
      <div className={classes.rootCheckout}>

        <Grid container direction="column">
          <Grid item xs={12}>
            <Grid container justify="center">
              <Hidden xsDown>
                <Navigator />
              </Hidden>
             
              <Grid item xs={11}>

                <Grid container direction="row" spacing={40}>
                  <Grid item md={7} xs={12}>
                    <BillingDetail 
                    user={user} 
                    errors={this.props.errors} 
                    email={email} 
                    user_information={user_information}
                    provinces={provinces}
                    regencies={regencies}
                    districts={districts}
                    villages={villages}
                    user_address={user_address}
                    handlerChangeUser={this.handlerChangeUser}
                    handlerChangeUserInfo={this.handlerChangeUserInfo}
                    handlerChangeAddress={this.handlerChangeAddress}
                    handlerChangeVillage={this.handlerChangeVillage}
                    handlerChangeDistrict={this.handlerChangeDistrict}
                    handleChangeRegency={this.handleChangeRegency}
                    handlerChangeProvince={this.handlerChangeProvince}
                  />
                  </Grid>
                  <Grid item md={5} xs={12}>
            
                    <ShippingMethod 
                      shippingFee={shippingFee}
                      handlerShippingMethod={this.handlerShippingMethod}
                      shippingMethod={shippingMethod}
                      />
                    <OrderDetail
                      shippingFee={shippingFee}
                      total={total}
                      carts={carts}
                      handlerSubmitOrder={this.handlerSubmitOrder}
                      vouchers={vouchers}
                    />
                  </Grid>
                </Grid>

              </Grid>
            </Grid>
          </Grid>
          <Dialog fullScreen open={loading}>

            <Grid container justify="center" alignItems="center" direction="column" style={{ height: "100%" }}>

              <CircularProgress className={classes.progress} />
              <Typography className={classes.normalText} style={{ margin: "20px 0" }}>Please wait Loading..</Typography>
            </Grid>

          </Dialog>
        </Grid>
   
      </div>
    )
  }
}


Checkout.propTypes = {
  products: PropTypes.object.isRequired,
  errors:PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  address:PropTypes.object.isRequired,
  auths:PropTypes.object.isRequired,
  getProvince:PropTypes.func.isRequired,
  getAddressLocation:PropTypes.func.isRequired,
  findRegencies: PropTypes.func.isRequired, 
  findDistricts: PropTypes.func.isRequired, 
  findVillages: PropTypes.func.isRequired,
  calculateCost:PropTypes.func.isRequired,
  shipping:PropTypes.object.isRequired,
  submitProceedToCheckout:PropTypes.func.isRequired,
  carts:PropTypes.object.isRequired,
  vouchers:PropTypes.object.isRequired
}

const mapStateToProps = state => (
  {
    products: state.products,
    carts:state.carts,
    errors:state.errors,
    auths:state.auths,
    address:state.address,
    shipping:state.shipping,
    vouchers:state.vouchers
    
  });

export default compose(connect(mapStateToProps, { 
  getProvince, 
  getAddressLocation, 
  findRegencies, 
  findDistricts, 
  findVillages,
  calculateCost,
  submitProceedToCheckout
}), withStyles(styles, { name: "Checkout" }))(withRouter(Checkout));
