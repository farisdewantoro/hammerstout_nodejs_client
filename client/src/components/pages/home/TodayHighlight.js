import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography'
import TabContainer from './TabContainer';
import withWidth from '@material-ui/core/withWidth';
import {compose} from 'redux'
import ProductList from '../../loader/ProductList';
const styles = theme => ({
    rootTodayHighlight: {
        [theme.breakpoints.up('md')]: {
            padding: 10
        }
    },
    titleHighlight: {
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        padding: "20px 0 20px 0",
        fontSize: "2.5em"
    },
    tabList: {
        minWidth: 'auto'
    }
});
class TodayHighlight extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            menuList: [
                {
                    name: "New Arrivals"
                }, {
                    name: "Jeans"
                }, {
                    name: "Pants"
                }, {
                    name: "Shirt"
                }, {
                    name: "T-Shirt"
                }, {
                    name: "Jacket"
                }, {
                    name: "Sweaters"
                }
            ]
        }
    }
    handleChange = (event, value) => {
        console.log(value);
        this.setState({value});
    };
    createLoadingProduct = ()=>{
        let loading=[];
        for (let index = 0; index < 8; index++) {
           loading.push(
               <Grid item md={3} xs={6} key={index}>
                   <ProductList />
               </Grid>
           )
            
        }
        return loading;
    }

    render() {

        const { classes, products, getProductWithParamCategory, width, category} = this.props;
        const {value} = this.state;
        // const loading = products.loading;
        // console.log();
        // let loadingBar;
        // if (loading) {
        //     loadingBar = ()
        // }
        return (
            <div>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <Grid
                            item
                            md={11}
                            xs={12}
                            style={{
                            marginTop: 100
                        }}>
                            <Paper className={classes.rootTodayHighlight}>
                           
                                <Typography variant="h5" className={classes.titleHighlight} component="h3">
                                    Today Highlight
                                </Typography>
                                <Tabs
                                    value={value}
                                    onChange={this.handleChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    centered={width === 'xl' || width === 'md' || width === 'lg'}
                                    scrollable={width !== 'xl' && width !== 'md' && width !== 'lg'}
                                    scrollButtons="auto"
                                    >
                                    
                                    {category.map((c, i) => {
                                        return (<Tab key={i} label={c.category} className={classes.tabList}/>)
                                    })}
                                </Tabs>
                                {category.length > 0 ? (<TabContainer
                                    tabIndex={value}
                                    menu={category}
                                    getProductWithParamCategory={getProductWithParamCategory}
                                    products={products}
                                ></TabContainer>):
                                (
                                    <div style={{padding:10}}>
                                        <Grid container direction="row" >
                                                {this.createLoadingProduct()}
                                        </Grid>
                                    </div>
                                )
                                }
                             

                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>

            </div>
        )
    }
}

TodayHighlight.propTypes = {
    classes: PropTypes.object.isRequired,
    getProductWithParamCategory: PropTypes.func.isRequired,
    products: PropTypes.object.isRequired,
    width: PropTypes.string.isRequired,
    category:PropTypes.array.isRequired
};

export default compose(withStyles(styles, {name: "TodayHighlight"}), withWidth())(TodayHighlight);
