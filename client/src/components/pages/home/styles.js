export default theme=>({
    containerNameLookbook:{
        background:"black"
    },
    lookbookNameText:{
        width:"100%",
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        fontSize:"1.5em"
    },
    titleLookbook:{
        fontFamily: "'Staatliches', cursive",
        [theme.breakpoints.up('md')]:{
            margin: "20px 0 20px 0",
        },
        [theme.breakpoints.down('sm')]: {
            margin: "0px 0 20px 0",
        },
    
        borderBottom:"3px solid black",
        fontSize: "2.5em",
        
    },
    titleInstagram:{
        fontFamily: "'Staatliches', cursive",
        [theme.breakpoints.up('md')]: {
            margin: "20px 0 20px 0",
            fontSize: "2.5em",
        },
        [theme.breakpoints.down('sm')]: {
            margin: "0px 0 20px 0",
            fontSize: "2em",
        },

        borderBottom: "3px solid black"
        
    },
    imageCollectionSingle:{
        Width:"100%"
    },
    rootInstagram: {
        display: 'flex',
        flexWrap: 'wrap',
        backgroundColor: theme.palette.background.paper,
        overflow:'hidden'
    },
    gridList: {
        width: '100%'
    },
 
})