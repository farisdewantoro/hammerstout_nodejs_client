import React,{Component} from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {compose} from 'redux'
import { withStyles } from '@material-ui/core/styles';
import {getProductWithParamCategory} from '../../../actions/productActions';
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
let styles=theme=>({
    containerGridProduct:{
        margin:theme.spacing.unit*3
    },
    productDetail:{
        margin:theme.spacing.unit
    },
    productTitle:{
        fontFamily: "'Staatliches', cursive",
        fontSize:18
    },
    productPricing: {
        display: 'flex',
        width: '100%'
    },
    regular_price: {
        marginRight: 5,
        fontWeight: 'bold'
    },
    isDiscount: {
        textDecoration: 'line-through'
    },
    discount_value: {
        color: '#e53935',
        fontWeight: 'bold',
        marginRight: 5,
        marginLeft: 5,
        
    },
    discountPercentage: {
        color: '#e53935',
        fontWeight: 'bold',
    },
    noDiscount:{
        opacity:0,
    }
})

class TabContainer extends Component {
    constructor(props){
        super(props);
        this.state={
            propsIndex:0,
            menuList:null,
            products:[]
        }
    }
    componentDidMount(){
        let propsIndex = this.props.tabIndex;
        let menuList = this.props.menu;
        this.props.getProductWithParamCategory(menuList[propsIndex].name);
    }
 
    componentWillReceiveProps(nextProps){
        let nextTabIndex = nextProps.tabIndex;
        let menuList = this.props.menu;
        if (this.props.tabIndex !== nextProps.tabIndex){
            this.props.getProductWithParamCategory(menuList[nextTabIndex].name);
        }
        if (this.props.products.todayHighlight !== nextProps.products.todayHighlight){
            this.setState({
                products: nextProps.products.todayHighlight
            })
        }
    }
    render(){
        let propsIndex = this.props.tabIndex;
        let menuList = this.props.menu;
        let {products} = this.state;
        let {classes} = this.props;
    return (
        <div className={classes.containerGridProduct}>
         
                <Grid container direction="row" spacing={16}>
                {products.map((p,i)=>{
                    return(
                        <Grid item md={3} key={i}>
                        <Card>
                            <CardActionArea>
                                    <img src={p.link} alt={p.alt} title={p.caption} style={{ width: '100%' }} />
                                <div className={classes.productDetail}>
                                        <Typography className={classes.productTitle}>
                                            {p.name}
                                        </Typography>
                                        <div className={classes.productPricing}>

                                            <span className={classNames(classes.regular_price, {
                                                [classes.isDiscount]: (p.discount_value !== null && p.discount_percentage !== null )
                                            })}>
                                                {`Rp. ${p.regular_price}`}
                                            </span>
                                            {(p.discount_value !== null && p.discount_percentage !== null && 
                                                <div>
                                                    <span className={classes.discount_value}>
                                                        {`Rp. ${p.discount_value}`}
                                                    </span>
                                                </div>
                                            )}
                                        

                                        </div>
                                        {(p.discount_value !== null && p.discount_percentage !== null ? 
                                            <div >
                                                <Typography component="p" className={classes.discountPercentage}>
                                                    {p.discount_percentage} % OFF
                                                </Typography>
                                            </div>
                                            :
                                            <div >
                                                <Typography component="p" className={classes.noDiscount}>
                                                    NO DISCOUNT
                                                </Typography>
                                            </div>
                                        )}
                                      
                                </div>
                                  
                           
                            </CardActionArea>
                        </Card>
                            
                        </Grid>
                    )
                })}
                    
                </Grid>
            
        </div>
    )
    }
 
}

TabContainer.propTypes = {
    tabIndex: PropTypes.number.isRequired,
    menu: PropTypes.array.isRequired,
    getProductWithParamCategory:PropTypes.func.isRequired,
    products:PropTypes.object.isRequired,
    classes:PropTypes.object.isRequired
};

const mapStateToProps= state=>({
    products:state.products
})

export default 
compose(
    connect(mapStateToProps, { getProductWithParamCategory}),
    withStyles(styles,{name:'TabContainer'})
    )
(TabContainer);
