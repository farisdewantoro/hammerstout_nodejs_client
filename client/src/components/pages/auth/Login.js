import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import keys from '../../../config/keys';
import { loginUser} from '../../../actions/authActions';
import {withRouter} from 'react-router';
import FormHelperText from '@material-ui/core/FormHelperText';
import ReactPixel from 'react-facebook-pixel';
 class Login extends Component {
   state={
     username:'',
     password:''
   }
   componentDidMount() {
     const options = {
       autoConfig: true, 	// set pixel's autoConfig
       debug: false, 		// enable logs
     };

     ReactPixel.init('365684027172803', options);
     ReactPixel.pageView();
 
   }
  handlerLoginWithGoogle = ()=>{
    window.location.href =keys.loginWith.google;
  }
  handlerLoginWithFacebook = ()=>{
    window.location.href = keys.loginWith.facebook;
  }
  handlerSubmitLogin = ()=>{
    this.props.loginUser(this.state,this.props.history);
  }
  handleChange = (e)=>{
    this.setState({
      [e.target.name]:e.target.value
    })
  }
  render() {
    const { classes, errors } = this.props;
    const { username,password} = this.state;
    return (
      <div className={classes.rootAuth}>
        <Grid container direction="column">
          <Grid item xs={12}>
            <Grid container justify="center">
              <Grid item md={10}>
                <Grid container direction="row" justify="center" spacing={40}>
                      <Grid item md={5}>
                    <Card>
                      <Typography variant="h1" className={classes.titleParams}>
                        SIGN IN
                    </Typography>
                      <Divider />
                      <CardContent>
                        <Grid container>
                          <Button fullWidth className={classes.loginGoogleWrapper} onClick={this.handlerLoginWithGoogle}>
                            <div className={classes.loginWithGoogle}>
                              <svg aria-hidden="true"  width="18" height="18" viewBox="0 0 18 18"><g><path d="M16.51 8H8.98v3h4.3c-.18 1-.74 1.48-1.6 2.04v2.01h2.6a7.8 7.8 0 0 0 2.38-5.88c0-.57-.05-.66-.15-1.18z" fill="#4285F4"></path><path d="M8.98 17c2.16 0 3.97-.72 5.3-1.94l-2.6-2a4.8 4.8 0 0 1-7.18-2.54H1.83v2.07A8 8 0 0 0 8.98 17z" fill="#34A853"></path><path d="M4.5 10.52a4.8 4.8 0 0 1 0-3.04V5.41H1.83a8 8 0 0 0 0 7.18l2.67-2.07z" fill="#FBBC05"></path><path d="M8.98 4.18c1.17 0 2.23.4 3.06 1.2l2.3-2.3A8 8 0 0 0 1.83 5.4L4.5 7.49a4.77 4.77 0 0 1 4.48-3.3z" fill="#EA4335"></path></g></svg>
                              <span style={{ margin: "0px 5px" }}> Login With Google</span>
                            </div>
                          </Button>
                        </Grid>
                        <Grid container justify="center" style={{ margin: "5px 0" }}>
                          <Typography >
                            OR
                            </Typography>
                        </Grid>
                        <Grid container>
                          <button type="button" className={classes.loginFacebookWrapper} onClick={this.handlerLoginWithFacebook}>
                            <FontAwesomeIcon icon={['fab', 'facebook']} size="lg" style={{ margin: "0px 10px" }} />
                            Login With  Facebook
                            </button>
                        </Grid>
                        <Grid container>
                          <TextField
                            id="email-address"
                            label="Email Adddress"
                            fullWidth
                            error={errors.message ? true:false}
                            name="username"
                            value={username}
                            onChange={this.handleChange}
                            margin="normal"
                            InputLabelProps={{
                              shrink: true,
                              classes: {
                                root: classes.textLabel
                              }
                            }}
                          />
                        </Grid>
                        <Grid container>
                          <TextField
                            id="password"
                            label="Password"
                            name="password"
                            value={password}
                            type="password"
                            onChange={this.handleChange}
                            // style={{ margin: 8 }}
                            fullWidth
                            error={errors.message ? true : false}
                            margin="normal"
                            InputLabelProps={{
                              shrink: true,
                              classes: {
                                root: classes.textLabel
                              }
                            }}
                          />
                        </Grid>
                        <Grid container>
                          <FormHelperText style={{
                            marginTop: 10
                          }} error={Object.keys(errors).length > 0 ? true : false}>
                            {errors.message}
                          </FormHelperText>
                        </Grid>
                        <Grid container style={{ margin: "20px 0" }} spacing={16} direction="row">
                     
                          
                          <Grid item>
                            <Button variant="contained" color="primary" onClick={this.handlerSubmitLogin}>
                              SIGN IN
                            </Button>
                          </Grid>

                          <Grid item>
                            <Button variant="contained" component={Link} to="/sign-up" color="primary">
                              SIGN UP
                            </Button>
                          </Grid>

                        </Grid>
                    
                        {/* <Grid container>
                          <Link to="forgot-password" style={{ textDecoration: "none" }}>
                            <Typography >
                              Forgot Password?
                              </Typography>
                          </Link>

                        </Grid> */}

                      </CardContent>
                    </Card>
                      </Grid>
                  {/* <Grid item md={5}>
                    <Register />
                  </Grid> */}
                  </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    )
  }
}

Login.propTypes = {
  auths: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  loginUser:PropTypes.func.isRequired,
  errors:PropTypes.object.isRequired
}

const mapStateToProps = state => (
  {
    auths: state.auths,
    errors:state.errors
  });

export default compose(
  connect(mapStateToProps,{loginUser}), 
  withStyles(styles, { name: "Login" }))
  (withRouter(Login));
