import React, {Component} from 'react'
import {connect} from 'react-redux'
import {compose} from 'redux'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import keys from '../../../config/keys';
import { registerNewUser} from '../../../actions/authActions';
import ReactPixel from 'react-facebook-pixel';
class Register extends Component {
    state={
        data:{
            displayName:'',
            email:'',
            password:'',
            confirmPassword: ''
        },
        error:false,
        errorMessage:''
    }
    componentDidMount() {
        const options = {
            autoConfig: true, 	// set pixel's autoConfig
            debug: false, 		// enable logs
        };

        ReactPixel.init('365684027172803', options);
        ReactPixel.pageView();

    }
    handlerLoginWithGoogle = () => {
        window.location.href = keys.loginWith.google;
    }
    handlerLoginWithFacebook = () => {
        window.location.href = keys.loginWith.facebook;
    }
    handlerRegisterSubmit = () =>{
        this.props.registerNewUser(this.state.data);
    }

    onChangeHandler = (e)=>{
        let name = e.target.name;
        let value = e.target.value;
        this.setState(prevState=>({
            data:{
                ...prevState.data,
                [name]:value
            }
        }))
    }

    shouldComponentUpdate(nextProps, nextState){
        let confirmPassword = nextState.data.confirmPassword;
        let password = this.state.data.password;
        if(confirmPassword !== this.state.data.confirmPassword){
            if (confirmPassword !== password) {
                this.setState({
                    error: true,
                    errorMessage: "Password doesn't match !"
                });
                return false;
            }else{
                this.setState({
                    error: false,
                    errorMessage:""
                });
                return false;
            }
        }
        if(nextState.data.password !== password && this.state.data.confirmPassword !== ''){
            if (this.state.data.confirmPassword !== nextState.data.password) {
                this.setState({
                    error: true,
                    errorMessage: "Password doesn't match !"
                });
                return false;
            } else {
                this.setState({
                    error: false,
                    errorMessage: ""
                });
                return false;
            }
       
        }
        return true;
     
    }
 

    render() {
        const { classes, errors} = this.props;
        const { displayName, email, password, confirmPassword} = this.state.data;
        const {error, errorMessage} = this.state;
      
       
        return (
            <div className={classes.rootAuth}>
                <Grid container direction="column">
                    <Grid item xs={12}>
                        <Grid container justify="center">
                            <Grid item md={11}>
                                <Grid container direction="row" justify="center" spacing={40}>
                                    <Grid item md={5}>
                                        <Card>
                                            <Typography variant="h1" className={classes.titleParams}>
                                                Register
                                            </Typography>
                                            <Divider/>
                                            <CardContent>
                                                <Grid container>
                                                    <Button fullWidth className={classes.loginGoogleWrapper} onClick={this.handlerLoginWithGoogle}>
                                                        <div className={classes.loginWithGoogle}>
                                                            <svg
                                                                aria-hidden="true"
                                                                class="svg-icon native iconGoogle"
                                                                width="18"
                                                                height="18"
                                                                viewBox="0 0 18 18">
                                                                <g>
                                                                    <path
                                                                        d="M16.51 8H8.98v3h4.3c-.18 1-.74 1.48-1.6 2.04v2.01h2.6a7.8 7.8 0 0 0 2.38-5.88c0-.57-.05-.66-.15-1.18z"
                                                                        fill="#4285F4"></path>
                                                                    <path
                                                                        d="M8.98 17c2.16 0 3.97-.72 5.3-1.94l-2.6-2a4.8 4.8 0 0 1-7.18-2.54H1.83v2.07A8 8 0 0 0 8.98 17z"
                                                                        fill="#34A853"></path>
                                                                    <path
                                                                        d="M4.5 10.52a4.8 4.8 0 0 1 0-3.04V5.41H1.83a8 8 0 0 0 0 7.18l2.67-2.07z"
                                                                        fill="#FBBC05"></path>
                                                                    <path
                                                                        d="M8.98 4.18c1.17 0 2.23.4 3.06 1.2l2.3-2.3A8 8 0 0 0 1.83 5.4L4.5 7.49a4.77 4.77 0 0 1 4.48-3.3z"
                                                                        fill="#EA4335"></path>
                                                                </g>
                                                            </svg>
                                                            <span
                                                                style={{
                                                                margin: "0px 5px"
                                                            }}>
                                                                Register with Google</span>
                                                        </div>
                                                    </Button>
                                                </Grid>
                                                <Grid
                                                    container
                                                    justify="center"
                                                    style={{
                                                    margin: "5px 0"
                                                }}>
                                                    <Typography >
                                                        OR
                                                    </Typography>
                                                </Grid>
                                                <Grid container>
                                                    <button type="button" className={classes.loginFacebookWrapper} onClick={this.handlerLoginWithFacebook}>
                                                        <FontAwesomeIcon
                                                            icon={['fab', 'facebook']}
                                                            size="lg"
                                                            style={{
                                                            margin: "0px 10px"
                                                        }}/>
                                                        Register with Facebook
                                                    </button>
                                                </Grid>

                                                <Grid container>
                                                    <TextField
                                                        label="Full Name"
                                                        fullWidth
                                                        name="displayName"
                                                        error={errors.displayName ? true:false }
                                                        helperText={errors.displayName ? errors.displayName :''}
                                                        value={displayName}
                                                        onChange={this.onChangeHandler}
                                                        margin="normal"
                                                        InputLabelProps={{
                                                        shrink: true,
                                                        classes: {
                                                            root: classes.textLabel
                                                        }
                                                    }}/>
                                                </Grid>
                                                <Grid container>
                                                    <TextField
                                                        label="Email Address"
                                                        fullWidth
                                                        name="email"
                                                        type="email"
                                                        value={email}
                                                        error={errors.email ? true : false}
                                                        helperText={errors.email ? errors.email : ''}
                                                        autoComplete="email"
                                                        onChange={this.onChangeHandler}
                                                        margin="normal"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                            classes: {
                                                                root: classes.textLabel
                                                            }
                                                        }} />
                                                </Grid>
                                                <Grid container>
                                                    <TextField
                                                        label="Password"
                                                        fullWidth
                                                        type="password"
                                                        name="password"
                                                        error={errors.password ? true : false}
                                                        helperText={errors.password ? errors.password : ''}
                                                        value={password}
                                                        onChange={this.onChangeHandler}
                                                        margin="normal"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                            classes: {
                                                                root: classes.textLabel
                                                            }
                                                        }} />
                                                </Grid>
                                                <Grid container>
                                                    <TextField
                                                        label="Confirm Password"
                                                        fullWidth
                                                        onChange={this.onChangeHandler}
                                                        error={error || errors.confirmPassword ? true : false}
                                                        helperText={errorMessage || errors.confirmPassword ? errors.confirmPassword : ''}
                                                        type="password"
                                                        name="confirmPassword"
                                                        value={confirmPassword}
                                                        margin="normal"
                                                        InputLabelProps={{
                                                            shrink: true,
                                                            classes: {
                                                                root: classes.textLabel
                                                            }
                                                        }} />
                                                </Grid>
                                                <Grid container style={{margin:"20px 0"}}>
                                                    <Button color="primary" variant="contained" fullWidth disabled={error} onClick={this.handlerRegisterSubmit}>
                                                        Register Now
                                                    </Button>
                                                </Grid>
                                                <Grid container alignItems="center">
                                                <Grid item>
                                                        <Typography>
                                                            Already have an account?
                                                        </Typography>
                                                </Grid>
                                                <Grid item>
                                                        <Button component={Link} to="/sign-in">
                                                            Sign In
                                                        </Button>
                                                </Grid>
                                                       
                                                     
                                                </Grid>
                                            </CardContent>
                                        </Card>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

Register.propTypes = {
    auths: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    registerNewUser:PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    auths: state.auths,
    errors:state.errors
});

export default compose(connect(mapStateToProps, { registerNewUser}), withStyles(styles, {name: "Register"}))(Register);
