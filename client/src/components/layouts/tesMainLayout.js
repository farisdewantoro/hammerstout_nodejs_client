import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import LogoHammer from '../../asset/logo/logo2.jpg';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import MenuIcon from '@material-ui/icons/Menu';
import DeleteIcon from '@material-ui/icons/Delete';
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
    faIgloo,
    faShoppingBag,
    faTags,
    faAngleRight,
    faSort,
    faTrashAlt,
    faUser,
    faSignature,
    faFileSignature,
    faHome,
    faCreditCard,
    faSignOutAlt
} from '@fortawesome/free-solid-svg-icons'
import {faFacebookF, faInstagram, faLine, faWhatsapp, faFacebook} from '@fortawesome/free-brands-svg-icons'
import Logo2 from '../../asset/logo/logo2.jpg'
import Button from '@material-ui/core/Button'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Badge from '@material-ui/core/Badge';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import {compose} from 'redux'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import {Link, Redirect} from 'react-router-dom'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import {connect} from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';
import classNames from 'classnames';
import {fetchCartList} from '../../actions/cartActions';
import {authSetUser} from '../../actions/authActions';
import axios from 'axios';
import Cookies from 'js-cookie';
import queryString from 'query-string';
import {withRouter} from 'react-router-dom';
import {fetchToken} from '../../actions/trackActions';
import Snackbars from '../common/Snackbars';
import Footer from './Footer';
library.add([
    faInstagram,
    faFacebookF,
    faFacebook,
    faLine,
    faWhatsapp,
    faShoppingBag,
    faTags,
    faAngleRight,
    faSort,
    faTrashAlt,
    faUser,
    faFileSignature,
    faHome,
    faCreditCard,
    faSignOutAlt
]);

const styles = theme => ({
    // root: {     flexGrow: 1 },
    content: {
        flexGrow: 1,
        marginTop: 90
    },
    appBar: {
        boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), -2px 1px 1px 0px rgba(0,0,0,0.14)',
        background: 'white'
    },
    appBarMobile: {
        boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), -2px 1px 1px 0px rgba(0,0,0,0.14)',
        background: 'white'
    },
    margin: {
        margin: theme.spacing.unit
    },
    wrapperInline: {
        display: 'inline-block'
    },
    wrapperIcon: {
        display: 'flex',
        alignItems: 'center'
    },
    logoHammer: {
        width: 200,
        margin: 5
    },
    logoHammerMobile: {
        width: 150
    },
    colorBlack: {
        color: 'black'
    },
    divider: {
        width: 1,
        height: 15
    },
    imageWrapper: {
        position: "absolute",
        left: "50%",
        transform: "translate(-50%,-50%)",
        top: "30%"
    },
    shoppingBagText: {
        textTransform: 'capitalize',
        fontSize: 12,
        fontWeight: 500,
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans - serif",
        // marginTop:3, marginLeft:5
        margin: theme.spacing.unit
    },
    badge: {
        top: -9,
        right: -20,
        // The border color match the background color.
        border: `1px solid ${theme.palette.type === 'light'
            ? theme.palette.grey[200]
            : theme.palette.grey[900]}`
    },
    bounceIn: {
        animation: `bounceIn 1.2s infinite`
    },
    '@keyframes heartBeat': {
        '0%': {
            transform: 'scale(1)'
        },
        '14%': {
            transform: 'scale(1.3)'
        },
        '28%': {
            transform: 'scale(1)'
        },
        '42%': {
            transform: 'scale(1.3)'
        },
        '70%': {
            transform: 'scale(1)'
        }
    },

    '@keyframes bounceIn': {
        "from": [
            '20%', '40%', '60%', '80%'
        ],

        "to": {
            animationTimingFunction: "cubic-bezier(0.215, 0.61, 0.355, 1)"
        },

        "0%": {
            opacity: 0,
            transform: "scale3d(0.3, 0.3, 0.3)"
        },
        "20%": {
            transform: "scale3d(1.1, 1.1, 1.1)"
        },
        "40%": {
            transform: "scale3d(0.9, 0.9, 0.9)"
        },
        "60%": {
            opacity: 1,
            transform: "scale3d(1.03, 1.03, 1.03)"
        },
        "80%": {
            transform: " scale3d(0.97, 0.97, 0.97)"
        },
        "to": {
            opacity: 1,
            transform: "scale3d(1, 1, 1)"
        }
    }

});

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: '#212121',
            dark: '#424242',
            contrastText: '#fff'
        },
        secondary: {
            main: '#fafafa'
        }
    },
    typography: {
        useNextVariants: true
    }
});

class MainLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            open: false,
            menuList: [
                {
                    name: "New Arrivals",
                    link: "/products/newarrivals"
                }, {
                    name: "Jeans",
                    link: "/products/jeans"
                }, {
                    name: "Pants",
                    link: "/products/pants"
                }, {
                    name: "Shirt",
                    link: "/products/shirt"
                }, {
                    name: "T-Shirt",
                    link: "/products/tshirt"
                }, {
                    name: "Jacket",
                    link: "/products/jacket"
                }, {
                    name: "Sweaters",
                    link: "/products/sweaters"
                }, {
                    name: "Accessories",
                    link: "/products/accessories"
                }
            ],
        
            left: false,
            cartList: [],
            totalQuantityCart: 0,
            notificationCart: false,
            status: null,
            prevPath: '',
            notification: {
                error: true,
                message: 'tes',
                openNotification: true
            }
        }
    }
    handleClick = event => {
        this.setState({open: true, anchorEl: event.currentTarget});
    };

    handleRequestClose = () => {
        this.setState({open: false});
    };
    toggleDrawer = (side, open) => () => {
        this.setState({[side]: open});
    };

    // componentWillUpdate(prevProps, prevState) {     if (prevState.status !==
    // this.state.status ) {         // else{         //
    // this.props.fetchToken();         //     this.props.fetchCartList();
    // // }     } }
    componentDidMount() {
        var query = queryString.parse(this.props.location.search);
        if (query.token && query.token !== "undefined") {
            this
                .props
                .authSetUser(query.token, this.props.history);
        } else {
            this
                .props
                .fetchToken();
            this
                .props
                .fetchCartList();
        }

        // axios.get('/api/v1/track')     .then(ress => {         let csrf =
        // Cookies.get('hammerstout_t');
        // axios.defaults.headers.common['csrf-token'] = csrf;         this.setState({
        //           status: ress.data         })     });

    }
    componentWillReceiveProps(nextProps) {
        let carts = nextProps.carts;
        let quantity = 0;

        if (carts !== this.props.carts) {
            this.setState({cartList: carts.cartList})

            carts
                .cartList
                .forEach(c => {
                    quantity = quantity + c.quantity
                });
            this.setState({totalQuantityCart: quantity, notificationCart: true})
            this.timeoutNotification(6000);
        }
        if (nextProps.notification !== this.props.notification) {
            let notif = nextProps.notification;
            this.setState(prevState => ({
                notification: {
                    ...prevState.notification,
                    error: notif.error,
                    message: notif.message,
                    openNotification: notif.notification
                }
            }))
        }
    }
    timeoutNotification = (time) => {
        setTimeout(this.removeNotification, time);
    }
    removeNotification = () => {
        this.setState({notificationCart: false})
    }

    render() {
        const {classes} = this.props;
        const {menuList, cartList, totalQuantityCart, notificationCart, notification} = this.state;
        const loadingProduct = this.props.products.loading;
        const loadingCart = this.props.carts.loading;
        const loadingAuth = this.props.auths.loading;
        const {user} = this.props.auths;
        const sideList = (
            <div className={classes.list}>

                <List>
                    {menuList.map((menu, i) => {
                        return (
                            <ListItem button key={menu.name} component={Link} to={menu.link}>

                                <ListItemText primary={menu.name}/>
                            </ListItem>
                        )
                    })}
                </List>
                <Divider/>

            </div>
        );
        let loadingContainer;
        if (loadingProduct || loadingCart || loadingAuth) {
            loadingContainer = (<LinearProgress color="primary"/>)
        }
        return (
            <MuiThemeProvider theme={theme}>

                <div className={classes.root}>

                    <Hidden smDown>

                        <AppBar position="fixed" color="default" className={classes.appBar}>
                            {loadingContainer}
                            <Toolbar>
                                <Grid container>
                                    <Grid
                                        item
                                        md={12}
                                        style={{
                                        marginTop: 5
                                    }}>
                                        <Grid container direction="row" justify="space-between">
                                            <Grid item>
                                                <div className={classes.wrapperInline}>
                                                    <IconButton aria-label="facebook" className={classes.colorBlack}>
                                                        <FontAwesomeIcon icon={['fab', 'facebook-f']} size="xs"/>
                                                    </IconButton>
                                                    <IconButton aria-label="instagram" className={classes.colorBlack}>
                                                        <FontAwesomeIcon icon={['fab', 'instagram']} size="xs"/>
                                                    </IconButton>
                                                    <IconButton aria-label="line" className={classes.colorBlack}>
                                                        <FontAwesomeIcon icon={['fab', 'line']} size="xs"/>
                                                    </IconButton>
                                                    <IconButton aria-label="whatsapp" className={classes.colorBlack}>
                                                        <FontAwesomeIcon icon={['fab', 'whatsapp']} size="xs"/>
                                                    </IconButton>

                                                </div>
                                            </Grid>
                                            <Grid item>
                                                <Grid container justify="center">
                                                    <Link to="/">
                                                        <div className={classes.imageWrapper}>
                                                            <img src={Logo2} alt="" className={classes.logoHammer}/>
                                                        </div>
                                                    </Link>

                                                </Grid>
                                            </Grid>

                                            <Grid item>
                                                <Grid container direction="row">
                                                    <Grid item>
                                                        <div className={classes.wrapperIcon}>
                                                            {user.email
                                                                ? (
                                                                    <Button component={Link} to="/my-account">
                                                                        {user.email}
                                                                    </Button>
                                                                )
                                                                : (
                                                                    <div className={classes.wrapperIcon}>
                                                                        <Button component={Link} to="/sign-in">
                                                                            Sign In
                                                                        </Button>
                                                                        <Divider className={classes.divider}/>
                                                                        <Button >
                                                                            Register
                                                                        </Button>
                                                                    </div>
                                                                )}

                                                            <Button
                                                                style={{
                                                                marginLeft: 5
                                                            }}
                                                                component={Link}
                                                                to="/carts">
                                                                <Badge
                                                                    badgeContent={totalQuantityCart}
                                                                    color="primary"
                                                                    classes={{
                                                                    badge: classNames(classes.badge, {
                                                                        [classes.bounceIn]: notificationCart
                                                                    })
                                                                }}>
                                                                    <FontAwesomeIcon icon={['fas', 'shopping-bag']} size="lg"/>

                                                                </Badge>

                                                            </Button>

                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>

                                    <Grid item md={12}>
                                        <Grid container direction="row" justify="center">
                                            {menuList.map((menu, i) => {
                                                return (
                                                    <Button key={i} component={Link} to={menu.link}>
                                                        {menu.name}
                                                    </Button>
                                                )
                                            })}
                                        </Grid>

                                    </Grid>
                                </Grid>

                            </Toolbar>
                        </AppBar>
                    </Hidden>

                    <Hidden mdUp>
                        <AppBar position="fixed" color="default" className={classes.appBarMobile}>
                            {loadingContainer}
                            <Toolbar>
                                <Grid container justify="space-between">
                                    <Hidden xsDown>
                                        <Grid item xs={12}>
                                            <Grid container direction="row" justify="space-between">
                                                <Grid item>
                                                    <div className={classes.wrapperIcon}>
                                                        <IconButton
                                                            color="inherit"
                                                            aria-label="Open drawer"
                                                            onClick={this.toggleDrawer('left', true)}>
                                                            <MenuIcon/>
                                                        </IconButton>
                                                        <Link to="/">
                                                            <img src={Logo2} alt="" className={classes.logoHammerMobile}/>

                                                        </Link>

                                                    </div>
                                                </Grid>
                                                <Grid item>
                                                    <div
                                                        className={classes.wrapperIcon}
                                                        style={{
                                                        marginTop: 5
                                                    }}>

                                                        <Button component={Link} to="/sign-in">
                                                            Sign In
                                                        </Button>
                                                        <Divider className={classes.divider}/>
                                                        <Button >
                                                            Register
                                                        </Button>

                                                        <Button component={Link} to="/carts">
                                                            <Badge
                                                                badgeContent={totalQuantityCart}
                                                                color="primary"
                                                                classes={{
                                                                badge: classNames(classes.badge, {
                                                                    [classes.bounceIn]: notificationCart
                                                                })
                                                            }}>
                                                                <FontAwesomeIcon icon={['fas', 'shopping-bag']} size="lg"/>
                                                            </Badge>

                                                        </Button>
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Hidden>

                                    <Hidden smUp>
                                        <Grid item xs={12}>
                                            <div className={classes.wrapperIcon}>
                                                <IconButton
                                                    color="inherit"
                                                    aria-label="Open drawer"
                                                    onClick={this.toggleDrawer('left', true)}>
                                                    <MenuIcon/>
                                                </IconButton>

                                                <Grid container justify="center">
                                                    <Link to="/">
                                                        <div className={classes.imageWrapper}>
                                                            <img
                                                                src={Logo2}
                                                                alt=""
                                                                style={{
                                                                maxHeight: 20,
                                                                maxWidth: '100%'
                                                            }}/>
                                                        </div>
                                                    </Link>

                                                </Grid>

                                            </div>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Grid container direction="row" justify="space-between" alignItems="center">
                                                <Grid item>
                                                    <div
                                                        className={classes.wrapperIcon}
                                                        style={{
                                                        marginTop: 5
                                                    }}>
                                                        <Button component={Link} to="/sign-in">
                                                            Sign In
                                                        </Button>
                                                        <Divider className={classes.divider}/>
                                                        <Button >
                                                            Register
                                                        </Button>
                                                    </div>
                                                </Grid>
                                                <Grid item>

                                                    <Button component={Link} to="/carts">
                                                        <Badge
                                                            badgeContent={totalQuantityCart}
                                                            color="primary"
                                                            classes={{
                                                            badge: classNames(classes.badge, {
                                                                [classes.bounceIn]: notificationCart
                                                            })
                                                        }}>

                                                            <FontAwesomeIcon icon={['fas', 'shopping-bag']} size="lg"/>
                                                        </Badge>

                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                        <SwipeableDrawer
                                            open={this.state.left}
                                            onClose={this.toggleDrawer('left', false)}
                                            onOpen={this.toggleDrawer('left', true)}>
                                            <div
                                                tabIndex={0}
                                                role="button"
                                                onClick={this.toggleDrawer('left', false)}
                                                onKeyDown={this.toggleDrawer('left', false)}>
                                                {sideList}
                                            </div>
                                        </SwipeableDrawer>
                                    </Hidden>

                                </Grid>
                            </Toolbar>
                        </AppBar>
                    </Hidden>

                    <main className={classes.content}>
                        {this.props.children}
                        <Snackbars notification={notification}/>
                    </main>
                    <Footer menuList={menuList}/>
                </div>
            </MuiThemeProvider>
        )
    }
}

MainLayout.propTypes = {
    classes: PropTypes.object.isRequired,
    products: PropTypes.object.isRequired,
    carts: PropTypes.object.isRequired,
    fetchCartList: PropTypes.func.isRequired,
    authSetUser: PropTypes.func.isRequired,
    fetchToken: PropTypes.func.isRequired,
    notification: PropTypes.object.isRequired
};

const mapStateToProps = state => ({products: state.products, carts: state.carts, auths: state.auths, tracks: state.tracks, notification: state.notification})

export default compose(withStyles(styles), connect(mapStateToProps, {fetchCartList, authSetUser, fetchToken}))(withRouter(MainLayout));