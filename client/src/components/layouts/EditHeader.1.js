import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import { Link, Redirect } from 'react-router-dom'
import styles from './styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Logo2 from '../../asset/logo/logo2.jpg'
// import Logo2 from '../../asset/logo/logo2.jpg'
import Divider from '@material-ui/core/Divider'
import Badge from '@material-ui/core/Badge';
import classNames from 'classnames';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Popover from '@material-ui/core/Popover';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
class Header extends Component {
    state={
        open:false,
        anchorEl: null,
    }
    handleToggle = (name) => {
        console.log(this.anchorEl)
        this.setState(state => ({ open: name }));
    };

    handleClose = event => {
       
        // if (this.anchorEl.contains(event.target)) {
           
        //     return;
        // }

        this.setState({ open: false });
    };
    handlePopoverOpen = (data)=>event => {
        
        this.setState(
            { 
                open: data,
                anchorEl: event.currentTarget 
             });
    };

    handlePopoverClose = (event) => {
   
        this.setState({ open: false });
        
    };
    handleStillOn = (data) =>(event)=>{
  
        let beforFrom = event.fromElement || event.fromElement;
        this.setState(
            {
                open: data,
                anchorEl: beforFrom
            });
    }

    render() {
        const { classes, menuList, totalQuantityCart, notificationCart, user, menuShop, loadingContainer } = this.props
        const { anchorEl, open } = this.state;
        // const open = Boolean(anchorEl);
        return (
            <div className={classes.wrapperHeader}>
                <AppBar position="fixed" color="default" className={classes.appBar}>
                    {loadingContainer}
                <Toolbar className={classes.toolbarTop}>
                    <Grid container>
                            <Grid item md={12} >
                                <Grid container direction="row" justify="space-between" alignItems="center">
                                    <Grid item>
                                        <div className={classes.wrapperInline}>
                                            <IconButton aria-label="facebook" className={classes.colorBlack}>
                                                <FontAwesomeIcon icon={['fab', 'facebook-f']} size="xs" />
                                            </IconButton>
                                            <IconButton aria-label="instagram" className={classes.colorBlack}>
                                                <FontAwesomeIcon icon={['fab', 'instagram']} size="xs" />
                                            </IconButton>
                                            <IconButton aria-label="line" className={classes.colorBlack}>
                                                <FontAwesomeIcon icon={['fab', 'line']} size="xs" />
                                            </IconButton>
                                            <IconButton aria-label="whatsapp" className={classes.colorBlack}>
                                                <FontAwesomeIcon icon={['fab', 'whatsapp']} size="xs" />
                                            </IconButton>

                                        </div>
                                    </Grid>

                                    <Grid item>
                                        <Grid container direction="row">
                                            <Grid item >
                                                <div className={classes.wrapperIcon}>
                                                    {user.email ? (<Button component={Link} to="/my-account">
                                                        {user.email}
                                                    </Button>) : (
                                                            <div className={classes.wrapperIcon}>
                                                                <Button component={Link} to="/sign-in">
                                                                    Sign In
                                                            </Button>
                                                                <Divider className={classes.divider} />
                                                                <Button >
                                                                    Register
                                                            </Button>
                                                            </div>
                                                        )}


                                                    <Button style={{ marginLeft: 5 }} component={Link} to="/carts">
                                                        <Badge badgeContent={totalQuantityCart} color="primary" classes={{
                                                            badge: classNames(classes.badge, {
                                                                [classes.bounceIn]: notificationCart
                                                            })
                                                        }}>
                                                            <FontAwesomeIcon icon={['fas', 'shopping-bag']} size="lg" />

                                                        </Badge>

                                                    </Button>
                                                </div>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                    </Grid>
                      
                </Toolbar>
                    <Toolbar>
                        <Grid container>

                           

                            <Grid item md={12}>
                                <div className={classes.hr}></div>
                            </Grid>
                        
                            <Grid item md={12} style={{ marginTop: 5,padding:10 }}>
                                <Grid container direction="row"  justify="center" alignItems="center" spacing={40}>
                             
                                    <Grid item md={2}>
                                    <Link to="/">
                                            <img src={Logo2} className={classes.imageLogo} alt="" />
                                    </Link>
                                        
                                    </Grid> 
                               

                                    <Grid item>
                                    <Grid container direction="row">
                                            
                                        <div>
                                            <Button
                                                aria-owns={open ? `menu-shop` : undefined}
                                                aria-haspopup="true"
                                                onMouseEnter={this.handlePopoverOpen('shop')}
                                                onMouseLeave={this.handlePopoverClose}
                                                className={classNames(classes.menuTitle, {
                                                    [classes.menuHovered]: (open == 'shop')
                                                })}
                                            >
                                                SHOP <KeyboardArrowDownIcon />
                                            </Button>
                                    
             
                                                      <Popper
                                                    onMouseEnter={this.handleStillOn('shop')}
                                                          onMouseLeave={this.handlePopoverClose}
                                                          id={`menu-shop`}
                                                          disablePortal
                                                          className={classes.popoverWrapper}
                                                    open={open === 'shop'}

                                                          anchorEl={anchorEl}
                                                          placement="bottom"

                                                      >

                                                          <Grow in={true}>
                                                              <Card className={classes.dropDownCard}>
                                                                  <CardContent>
                                                                      <Grid container direction="row" spacing={40}>
                                                                          {menuShop.listParent.map((lp, indexLP) => {
                                                                              return (

                                                                                  <Grid item key={indexLP}>
                                                                                      <Grid container direction="column" spacing={8}>
                                                                                          <Grid item>
                                                                                              <Typography className={classes.textListParent}>
                                                                                                  {lp.name}
                                                                                              </Typography>
                                                                                          </Grid>
                                                                                          <Grid item>
                                                                                              <Grid container direction="column" spacing={8}>
                                                                                                  {lp.listChild.map((lc, indexLC) => {
                                                                                                      return (
                                                                                                          <Grid item>

                                                                                                                
                                                                                                              <Link className={classes.linkMenu} to={lc.link}>
                                                                                                                  {lc.name}
                                                                                                              </Link>


                                                                                                          </Grid>
                                                                                                      )

                                                                                                  })}
                                                                                              </Grid>
                                                                                          </Grid>



                                                                                      </Grid>

                                                                                  </Grid>


                                                                              )
                                                                          })}
                                                                      </Grid>


                                                                  </CardContent>
                                                              </Card>
                                                          </Grow>



                                                      </Popper>
                                                </div>
                                              <div>
                                                <Button
                                                component={Link}
                                                to="/products/newarrivals"
                                                    aria-owns={open ? `menu-newarrivals` : undefined}
                                                    aria-haspopup="true"
                                                    onMouseEnter={this.handlePopoverOpen('newarrivals')}
                                                    onMouseLeave={this.handlePopoverClose}
                                                    className={classNames(classes.menuTitle, {
                                                        [classes.menuHovered]: (open == 'newarrivals')
                                                    })}
                                                >
                                                    NEW ARRIVALS
                                            </Button>
                                              </div>     
                                              <div>
                                                <Button
                                                    aria-owns={open ? `menu-collections` : undefined}
                                                    aria-haspopup="true"
                                                    onMouseEnter={this.handlePopoverOpen('collections')}
                                                    onMouseLeave={this.handlePopoverClose}
                                                    className={classNames(classes.menuTitle, {
                                                        [classes.menuHovered]: (open == 'collections')
                                                    })}
                                                >
                                                    COLLECTIONS
                                            </Button>
                                             </div>  
                                            <div>
                                                <Button
                                                    aria-owns={open ? `menu-aboutus` : undefined}
                                                    aria-haspopup="true"
                                                    onMouseEnter={this.handlePopoverOpen('aboutus')}
                                                    onMouseLeave={this.handlePopoverClose}
                                                    className={classNames(classes.menuTitle, {
                                                        [classes.menuHovered]: (open == 'aboutus')
                                                    })}
                                                >
                                                    ABOUT US
                                            </Button>
                                            </div>  
                                            <div>
                                                <Button
                                                    aria-owns={open ? `menu-lookbook` : undefined}
                                                    aria-haspopup="true"
                                                    onMouseEnter={this.handlePopoverOpen('menu-lookbook')}
                                                    onMouseLeave={this.handlePopoverClose}
                                                    className={classNames(classes.menuTitle, {
                                                        [classes.menuHovered]: (open == 'menu-lookbook')
                                                    })}
                                                    component={Link}
                                                    to="/lookbook"
                                                >
                                                    LOOKBOOK
                                            </Button>
                                            </div>                       
                                     
                                            
                                            
                                           
                                       
                                    </Grid>
                                
                                        
                                </Grid>
                                    {/* <Grid item md={3}>
                                    </Grid>   */}

                            </Grid>
                            </Grid>

                        </Grid>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
    menuList: PropTypes.array.isRequired,
    category:PropTypes.array.isRequired,
    category_tag:PropTypes.array.isRequired
};
export default withStyles(styles)(Header);
