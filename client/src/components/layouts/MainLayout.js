import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {library} from '@fortawesome/fontawesome-svg-core'
import {faShoppingBag,faTags,faAngleRight,faSort,faTrashAlt, faUser, faFileSignature, faHome, faCreditCard, faSignOutAlt, faMinus, faUserCircle, faEye, faAngleLeft} from '@fortawesome/free-solid-svg-icons'
import {faFacebookF, faInstagram, faLine, faWhatsapp, faFacebook} from '@fortawesome/free-brands-svg-icons'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import {compose} from 'redux'
import {connect} from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';
import { fetchCartList } from '../../actions/cartActions';
import { authSetUser, getUserInfo} from '../../actions/authActions';
import {getInstagramMedia} from '../../actions/instagramActions';
import queryString from 'query-string';
import { withRouter } from 'react-router-dom';
import Snackbars from '../common/Snackbars';
import Footer from './Footer';
import Header from './Header';
import styles from './styles';
library.add([faInstagram, 
    faFacebookF, 
    faFacebook,faLine, 
    faWhatsapp, faShoppingBag, faTags, 
    faAngleLeft,
    faAngleRight, faSort, faTrashAlt,
    faUser,faFileSignature,faHome,faCreditCard,
    faSignOutAlt,
    faUserCircle,
    faMinus,
    faEye
    ]);







const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: '#212121',
            dark: '#424242',
            contrastText: '#fff',
        },
        secondary:{
            main: '#fafafa',
        }
    },
    typography: {
        useNextVariants: true,
    },
});



class MainLayout extends Component {
    constructor(props){
        super(props);
        this.state={
            anchorEl: null,
            open: false,
            menuList:[
                { name: "New Arrivals",link:"/products/newarrivals"},
                { name: "Jeans", link: "/products/jeans"},
                { name: "Pants", link: "/products/pants" },
                { name: "Shirt", link: "/products/shirt" },
                { name: "T-Shirt", link: "/products/tshirt" },
                { name: "Jacket", link: "/products/jacket" },
                { name: "Sweaters", link: "/products/sweaters" },
                { name: "Accessories", link: "/products/accessories" },
            ],
            menuShop:{
                    name: "SHOP",
                    link: "/products",
                    listParent: [
                        {
                            name: "TOPS",
                            listChild: [
                                {
                                    name: "T-Shirts",
                                    link: "/products/tshirt"
                                },
                                {
                                    name: "Knits",
                                    link: "/products/Knits"
                                },
                                {
                                    name: "Jackets",
                                    link: "/products/jacket"
                                },
                                {
                                    name: "Sweatshirts",
                                    link: "/products/sweatshirts"
                                }
                            ]
                        },
                        {
                            name:"HEADWEAR",
                            listChild: [
                                {
                                    name: "Dad hats",
                                    link: "/products/tshirt"
                                },
                                {
                                    name: "SnapBacks",
                                    link: "/products/Knits"
                                },
                                {
                                    name: "Jackets",
                                    link: "/products/jacket"
                                },
                                {
                                    name: "Sweatshirts",
                                    link: "/products/sweatshirts"
                                }
                            ]
                        },
                        {
                            name:"BOTTOMS",
                            listChild: [
                                {
                                    name: "Dad hats",
                                    link: "/products/tshirt"
                                },
                                {
                                    name: "SnapBacks",
                                    link: "/products/Knits"
                                },
                                {
                                    name: "Jackets",
                                    link: "/products/jacket"
                                },
                                {
                                    name: "Sweatshirts",
                                    link: "/products/sweatshirts"
                                }
                            ]
                        },
                        {
                            name:"ACCESSORIES",
                            listChild: [
                                {
                                    name: "Footwear",
                                    link: "/products/tshirt"
                                },
                                {
                                    name: "SnapBacks",
                                    link: "/products/Knits"
                                },
                                {
                                    name: "Jackets",
                                    link: "/products/jacket"
                                },
                                {
                                    name: "Sweatshirts",
                                    link: "/products/sweatshirts"
                                }
                            ]
                        }
                    ]
                },
            left: false,
            cartList:[],
            totalQuantityCart:0,
            notificationCart:false,
            status:null,
            prevPath: '',
            notification: {
                error: true,
                message: 'tes',
                openNotification: true
            },
            openDrawer:false
        }
    }
    handleClick = event => {
        this.setState({ open: true, anchorEl: event.currentTarget });
    };

    handleRequestClose = () => {
        this.setState({ open: false });
    };
    toggleDrawer = (side, open) => (e) => {
        console.log(e.target);

        this.setState({
            [side]: open,
        });
    };
    
   
    // componentWillUpdate(prevProps, prevState) {
    //     if (prevState.status !== this.state.status ) {
           
         
    //         // else{
    //         //     this.props.fetchToken();
    //         //     this.props.fetchCartList();
    //         // }
           
    //     }
    // }
    componentDidMount(){
     
        var query = queryString.parse(this.props.location.search);
        if (query.token && query.token !== "undefined") {
            this.props.authSetUser(query.token, this.props.history);
        } else {
            
            this.props.fetchCartList();
            this.props.getUserInfo();
        }
        this.props.getInstagramMedia();
      
        // axios.get('/api/v1/track')
        //     .then(ress => {
        //         let csrf = Cookies.get('hammerstout_t');
        //         axios.defaults.headers.common['csrf-token'] = csrf;
        //         this.setState({
        //             status: ress.data
        //         })
        //     });

     
       
    }
    componentWillReceiveProps(nextProps){
        let carts = nextProps.carts;
        let quantity = 0;
   
        if(carts !== this.props.carts){
            this.setState({
                cartList:carts.cartList
            })

            carts.cartList.forEach(c => {
               quantity = quantity+c.quantity
            });
            this.setState({
                totalQuantityCart:quantity,
                notificationCart:true
            })
            this.timeoutNotification(6000);
        }
        if (nextProps.notification !== this.props.notification){
            let notif = nextProps.notification;
            this.setState(prevState=>({
                notification:{
                    ...prevState.notification,
                    error: notif.error,
                    message:notif.message,
                    openNotification:notif.notification
                }
            }))
        }
    }
    timeoutNotification = (time)=> {
        setTimeout(this.removeNotification, time);
    }
    removeNotification = ()=> {
        this.setState({
            notificationCart: false
        })
    }
   
    render() {
        const {classes} = this.props;
        const { menuList, totalQuantityCart, notificationCart, notification, menuShop, openDrawer} = this.state;
        const loadingProduct = this.props.products.loading;
        const loadingCart = this.props.carts.loading;
        const loadingAuth = this.props.auths.loading;
        const loadingOrder = this.props.orders.loading;
        const {user} = this.props.auths;
        const {category,category_tag,collection}=this.props.UI;
       
        let loadingContainer;
        if (loadingProduct || loadingCart || loadingAuth || loadingOrder){
            loadingContainer =(
                <LinearProgress color="primary" />
            )
        }
        return (
            <MuiThemeProvider theme={theme}>
                
            <div className={classes.root}>
                    <Header 
                    totalQuantityCart={totalQuantityCart}
                    notificationCart={notificationCart}
                    user={user}
                    menuShop={menuShop}
                    loadingContainer={loadingContainer}
                    category={category}
                    category_tag={category_tag}
                    toggleDrawer={this.toggleDrawer}
                    openDrawer={openDrawer}
                    collection={collection}/>
    
                     
    
    
                <main className={classes.content}>
                    {this.props.children}
                    <Snackbars notification={notification} />
                </main>
                    <Footer menuList={menuList}/>
            </div>
             </MuiThemeProvider>   
        )
    }
}

MainLayout.propTypes = {
    classes: PropTypes.object.isRequired,
    products:PropTypes.object.isRequired,
    carts:PropTypes.object.isRequired,
    fetchCartList:PropTypes.func.isRequired,
    authSetUser:PropTypes.func.isRequired,
    notification:PropTypes.object.isRequired,
    getUserInfo:PropTypes.func.isRequired,
    UI:PropTypes.object.isRequired,
    orders:PropTypes.object.isRequired,
    getInstagramMedia:PropTypes.func.isRequired
};

const mapStateToProps =state=>({
    products:state.products,
    carts:state.carts,
    auths:state.auths,
    tracks:state.tracks,
    notification:state.notification,
    UI:state.UI,
    orders: state.orders
})

export default compose(withStyles(styles), connect(mapStateToProps, { fetchCartList, authSetUser, getInstagramMedia, getUserInfo}))(withRouter(MainLayout));