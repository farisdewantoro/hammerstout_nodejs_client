import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import {Link, Redirect} from 'react-router-dom'
import styles from './styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
class Footer extends Component {
    render() {
        const {classes, menuList} = this.props
        return (
            <div className={classes.wrapperFooter}>
                <Grid container justify="center">
                    <Grid
                        item
                        md={10}
                        style={{
                        margin: "20px 0"
                    }}>
                        <Grid container spacing={16}>
                            <Grid item md={3}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignContent="center"
                                    alignItems="center">
                                    <Typography className={classes.titleTop}>
                                        OFFICE & HOMESTORE
                                    </Typography>
                                    <Typography className={classes.office}>
                                       Jln. Sari Indah IV no.19, Babakan Sari, Kiaracondong, Bandung. 
                                        Monday – Sunday (10.00 – 20.00 WIB)
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item md={2}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignContent="center"
                                    alignItems="center">
                                    <Typography className={classes.titleTop}>
                                        SHOP
                                    </Typography>
                                    <ul>
                                        {menuList.map((m, i) => {
                                            return (
                                                <li className={classes.listShop}>
                                                    <Link to={m.link} className={classes.link}>
                                                        {m.name}
                                                    </Link>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </Grid>
                            </Grid>

                            <Grid item md={2}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignContent="center"
                                    alignItems="center">
                                    <Typography className={classes.titleTop}>
                                        HELP
                                    </Typography>
                                    <ul>
                                        <li className={classes.listShop}>
                                            <Link to="/payment-guide" className={classes.link}>Payment Guide</Link>
                                        </li>
                                        <li className={classes.listShop}>
                                            <Link to="/size-guide" className={classes.link}>Size Guide</Link>
                                        </li>
                                        <li className={classes.listShop}>
                                            <Link to="/size-guide" className={classes.link}>Contact</Link>
                                        </li>
                                        <li className={classes.listShop}>
                                            <Link to="/size-guide" className={classes.link}>Find a Store</Link>
                                        </li>
                                    </ul>
                                </Grid>
                            </Grid>
                            <Grid item md={2}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignContent="center"
                                    alignItems="center">
                                    <Typography className={classes.titleTop}>
                                        HELP
                                    </Typography>
                                    <ul>
                                        <li className={classes.listShop}>
                                            <Link to="/payment-guide" className={classes.link}>Payment Guide</Link>
                                        </li>
                                        <li className={classes.listShop}>
                                            <Link to="/size-guide" className={classes.link}>Size Guide</Link>
                                        </li>
                                        <li className={classes.listShop}>
                                            <Link to="/size-guide" className={classes.link}>Contact</Link>
                                        </li>
                                        <li className={classes.listShop}>
                                            <Link to="/size-guide" className={classes.link}>Find a Store</Link>
                                        </li>
                                    </ul>
                                </Grid>
                            </Grid>
                            
                            <Grid item md={3}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignContent="center"
                                    alignItems="center">
                                    <Typography className={classes.titleTop}>
                                       CONNECT WITH US
                                    </Typography>
                                    <div className={classes.wrapperIcon}>
                                        <IconButton aria-label="facebook" className={classes.colorWhite}>
                                            <FontAwesomeIcon icon={['fab', 'facebook-f']} size="md" />
                                        </IconButton>
                                        <IconButton aria-label="instagram" className={classes.colorWhite}>
                                            <FontAwesomeIcon icon={['fab', 'instagram']} size="md" />
                                        </IconButton>
                                        <IconButton aria-label="line" className={classes.colorWhite}>
                                            <FontAwesomeIcon icon={['fab', 'line']} size="md" />
                                        </IconButton>
                                        <IconButton aria-label="whatsapp" className={classes.colorWhite}>
                                            <FontAwesomeIcon icon={['fab', 'whatsapp']} size="md" />
                                        </IconButton>
                                    </div>
                                
                                </Grid>
                            </Grid>
                         
                            

                        </Grid>
                                     
                    </Grid>
                    <Grid container justify="center">
                        <Typography className={classes.colorWhite}>
                            Hammerstout © 2018 Developed by DevF. All Rights Reserved.
                             </Typography>
                    </Grid>  
                </Grid>
            </div>
        )
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
    menuList: PropTypes.array.isRequired
};
export default withStyles(styles)(Footer);
