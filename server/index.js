import express from 'express';
import bodyParser from 'body-parser';
import passport from 'passport';
import connection from './config/conn';
import { 
    CategoryRoutes,
    ProductRoutes,
    CartRoutes,
    AuthRoutes,
    TrackRoutes,
    AddressRoutes,
    ShippingRoutes,
    UIRoutes,
    LookbookRoutes,
    CheckoutRoutes,
    OrderRoutes,
    VoucherRoutes,
    CollectionRoutes,
    v1Routes,
    SizingRoutes,
    InstagramRoutes
} from './modules';
import session  from 'express-session';
import cors from 'cors'
import cookieParser  from 'cookie-parser'
import passportSetup from './config/passport-setup';
import keys from './config/keys';
import uuidv4  from 'uuid/v4';
import UAparser  from 'ua-parser-js';
import { ensureSession } from './config/sessionCheck';
import path from 'path';

const app = express();
app.use(express.static('client/build'));

app.use(session({
    genid: function (req) {
        return uuidv4() // use UUIDs for session IDs
    },
    name:keys.session.name,
    secret: keys.session.secret,
    resave: false,
    saveUninitialized: true,
    rolling:true,
    cookie: { 
        secure: false,
        httpOnly: true,
        maxAge:keys.session.maxAge, // satu hari,
        sameSite:true,
     }
 
}));


// app.use('/public',express.static(path.join(__dirname,'public')));

app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());
// app.use(csrf({ cookie: false }));

app.disable('x-powered-by');

app.use(cors({ origin: keys.origin.url, credentials: true  }))



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('X-XSS-Protection', '1; mode=block');
    res.header('X-Frame-Options', 'deny');
    res.header('X-Content-Type-Options', 'nosniff');
    res.header("Access-Control-Allow-Origin", keys.origin.url);
 
    next();     
})


// app.get('/', function (req, res, next) {
    
//     var ua = UAparser(req.headers['user-agent']);
//     res.end(JSON.stringify(ua, null, '  '));
// })
app.use('/api/', ensureSession, [
    CategoryRoutes, 
    ProductRoutes, 
    CartRoutes, 
    AuthRoutes, 
    TrackRoutes, 
    AddressRoutes, 
    ShippingRoutes, 
    UIRoutes,
    LookbookRoutes,
    CheckoutRoutes,
    OrderRoutes,
    VoucherRoutes,
    CollectionRoutes,
    SizingRoutes,
    InstagramRoutes
]);

app.use('/v1/', [v1Routes]);
if (process.env.NODE_ENV === 'production') {
    // Set static folder



    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, '../', 'client', 'build', 'index.html'));
    })
}

// Server static assets if in production



const port = process.env.PORT || 5000;
app.listen(port, (err) => {
    if(err){
        console.log(err);
    }else{
        console.log(`Server running on port ! ${port}`);
    }
    
});

