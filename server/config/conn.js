const mysql = require('mysql');

var con = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'hammerst_hammer',
    multipleStatements: true
});

con.connect((err) =>{
    if(err) throw err;
    else console.log('MySql connected')
});

module.exports = con;