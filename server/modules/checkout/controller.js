import db from '../../config/conn';
import async from 'async';
import midtransClient from 'midtrans-client';
import keys from '../../config/keys';
import jwt from 'jsonwebtoken';

export const insertPaymentOrder = (req,res)=>{
 
    if (req.params.token_order == null 
        || req.params.token_order == '' || 
        typeof req.params.token_order === "undefined") {
        let notification = {
            error: true,
            message: "There is an error !",
            notification: true
        }
        return res.status(400).json({ notification: notification });
    }
    let order_id = jwt.verify(req.params.token_order, keys.jwt.secretOrPrivateKey2);
    if(order_id.uniqueID !== req.body.order_id){
        let notification = {
            error: true,
            message: "There is an error !",
            notification: true
        }
        return res.status(400).json({ notification: notification });
    }
    if (req.body.fraud_status == "deny"){
        let notification = {
            error: true,
            message: "There is an error !",
            notification: true
        }
        return res.status(400).json({ notification: notification });
    }
    // TODO::VALIDATION REQ.BODY
    let queryOrderPayment = `INSERT into order_payment set ? `;
    let dataOrderPayment = {};
    const queryFindOrderPayment = `SELECT * from order_payment where order_id = '${req.body.order_id}'`;
    const queryUpdateOrderPayment = `UPDATE order_payment set ? where order_id='${req.body.order_id}'`;
    db.query(queryFindOrderPayment,(err,result)=>{
        if(err){
            let notification = {
                error: true,
                message: "There is an error !",
                notification: true
            }
            return res.status(400).json({ notification: notification });
        }
        if(result.length > 0){
            Object.keys(req.body).forEach(rb => {
                if (
                    rb === "fraud_status" ||
                    rb === "payment_type" ||
                    rb === "finish_redirect_url" ||
                    rb === "status_code" ||
                    rb === "transaction_id" ||
                    rb === "transaction_status" ||
                    rb === "transaction_time" ||
                    rb === "pdf_url") {

                    dataOrderPayment[rb] = req.body[rb];
                }
                if (rb === "gross_amount") {
                    dataOrderPayment[rb] = parseInt(req.body[rb]);
                }
            });

            if (Object.keys(dataOrderPayment.length > 0)) {
                db.query(queryUpdateOrderPayment, [dataOrderPayment], (err, result) => {
                    if (err) {
                        let notification = {
                            error: true,
                            message: "There is an error !",
                            notification: true
                        }
                        return res.status(400).json(err);
                    }
                    if (result) {
                        let notification = {
                            error: false,
                            message: "Your payment has been process !",
                            notification: true
                        }
                        let pdf_url;
                        if (typeof dataOrderPayment.pdf_url !== "undefined" && dataOrderPayment.pdf_url.length > 0) {
                            pdf_url = dataOrderPayment.pdf_url;
                        }
                        return res.status(200).json({ notification: notification, token: req.params.token_order, pdf_url: pdf_url });
                    }
                })
            }
            if (Object.keys(dataOrderPayment).length == 0) {
                let notification = {
                    error: true,
                    message: "There is an error !",
                    notification: true
                }
                return res.status(400).json({ notification: notification });
            }
        }
        if(result.length === 0){
            Object.keys(req.body).forEach(rb => {
                if (
                    rb === "fraud_status" ||
                    rb === "payment_type" ||
                    rb === "finish_redirect_url" ||
                    rb === "status_code" ||
                    rb === "transaction_id" ||
                    rb === "transaction_status" ||
                    rb === "transaction_time" ||
                    rb === "order_id" ||
                    rb === "pdf_url") {

                    dataOrderPayment[rb] = req.body[rb];
                }
                if (rb === "gross_amount") {
                    dataOrderPayment[rb] = parseInt(req.body[rb]);
                }
            });

            if (Object.keys(dataOrderPayment.length > 0)) {
                db.query(queryOrderPayment, [dataOrderPayment], (err, result) => {
                    if (err) {
                        let notification = {
                            error: true,
                            message: "There is an error !",
                            notification: true
                        }
                        return res.status(400).json(err);
                    }
                    if (result) {
                        let notification = {
                            error: false,
                            message: "Your payment has been process !",
                            notification: true
                        }
                        let pdf_url;
                        if (typeof dataOrderPayment.pdf_url !== "undefined" && dataOrderPayment.pdf_url.length > 0) {
                            pdf_url = dataOrderPayment.pdf_url;
                        }
                        return res.status(200).json({ notification: notification, token: req.params.token_order, pdf_url: pdf_url });
                    }
                })
            }
            if (Object.keys(dataOrderPayment).length == 0) {
                let notification = {
                    error: true,
                    message: "There is an error !",
                    notification: true
                }
                return res.status(400).json({ notification: notification });
            }
        }
    })


    

    

}

export const submitPayment =(req,res)=>{
    // TODO :: VALIDATION REQ.BODY
    if (req.params.token_order == null || req.params.token_order == '' || typeof req.params.token_order === "undefined") {
        let notification = {
            error: true,
            message: "There is an error !",
            notification: true
        }
        return res.status(400).json({ notification: notification });
    }
    let order_id = jwt.verify(req.params.token_order, keys.jwt.secretOrPrivateKey2);


    let queryOrder = `SELECT 
    ord.id,
    ord.user_id,
    ors.status,
    ors.id as order_status_id
    ,ord.created_at 
    from orders as ord 
    left join order_status as ors on ord.order_status_id = ors.id
    where ord.user_id = ${req.user.id} && ord.id = '${order_id.uniqueID}' 
    && ord.order_status_id != 2 `;

    let queryOrderItems = `SELECT 
    p.name as product_name,
    p.slug as product_slug,
    p.description,
    oi.price,
    oi.order_id,
    c.name as category_name,
    c.slug,
    ct.name as category_type,
    ct.slug as category_type_slug,
    p.id as product_id,
    pa.id as product_attribute_id,
    pv.id as product_variant_id,
    i.link,
    i.caption,
    i.alt,
    pa.size,
    oi.quantity 
    from order_item as oi 
    left join orders as ord on oi.order_id = ord.id
    left join products as p on oi.product_id = p.id
    left join product_category as pc on p.id = pc.product_id 
    left join categories as c on pc.category_id = c.id 
    left join product_attribute as pa on oi.product_attribute_id = pa.id
    left join product_variant as pv on oi.product_variant_id = pv.id
    left join category_type as ct on pv.category_type_id = ct.id
    left join product_image as pi on pi.id = (SELECT pi1.id from product_image as pi1 where pi1.product_id = p.id order by pi1.product_id asc limit 1)
    left join images as i on pi.image_id = i.id
    where ord.user_id = ${req.user.id} && ord.id = '${order_id.uniqueID}'
    && ord.order_status_id != 2 `;

    let queryOrderShipment = `SELECT 
    os.courier,
    os.description,
    os.service,
    os.cost,
    os.etd,
    ord.id as order_id 
    from order_shipment as os
    left join orders as ord on os.order_id = ord.id
    where ord.user_id = ${req.user.id} && ord.id = '${order_id.uniqueID}' 
    && ord.order_status_id != 2 `;

    let queryOrderVoucher = `SELECT
    v.id as voucher_id,
    v.name as voucher_name,
    v.voucher_type_id as voucher_type,
    v.value,
    ov.order_id
    from order_voucher as ov
    left join vouchers as v on ov.voucher_id = v.id
    left join orders as ord on ov.order_id = ord.id
    where ord.user_id = ${req.user.id} && ord.id = '${order_id.uniqueID}'
    && ord.order_status_id != 2 
    `;

    let queryOrderBill = `SELECT 
    ob.order_id,
    ob.firstname,
    ob.lastname,
    ob.email,
    ob.phone_number,
    ob.province,
    ob.regency,
    ob.district,
    ob.village,
    ob.postcode,
    ob.address
     from order_billing as ob 
     left join orders as ord on ob.order_id = ord.id 
     where ob.order_id = '${order_id.uniqueID}' && ord.order_status_id != 2
     `;



    async.parallel({
        orders: function (callback) {
            db.query(queryOrder, (err, result) => {
                callback(err, result);
            })
        },
        order_item: function (callback) {
            db.query(queryOrderItems, (err, result) => {
                callback(err, result);
            })
        },
        order_shipment: function (callback) {
            db.query(queryOrderShipment, (err, result) => {
                callback(err, result);
            })
        },
        order_voucher: function (callback) {
            db.query(queryOrderVoucher, (err, result) => {
                callback(err, result);
            })
        },
        order_billing: function (callback) {
            db.query(queryOrderBill, (err, result) => {
                callback(err, result);
            })
        },
      
    }, function (err, result) {
        if (err) {
            let notification = {
                error: true,
                message: "There is an error !",
                notification: true
            }
            return res.status(400).json(err);
        }
        if (result) {
        
            let total = 0;
            let subTotal = 0;
            let shipmentFee = 0;
            subTotal = result.order_item.map(oi=>oi.price).reduce((a,b)=>a+b);
            shipmentFee = result.order_shipment.map(os=>os.cost);
            total = parseInt(subTotal) + parseInt(shipmentFee);
            if (result.order_voucher instanceof Array && result.order_voucher.length > 0 && typeof result.order_voucher !== "undefined") {
            if (result.order_voucher[0].voucher_type == 1) {
                let discount = total * (result.order_voucher[0].value / 100);
                 total = Math.max(1, total - discount);
                }
            if (result.order_voucher[0].voucher_type == 2) {
               total = Math.max(1, total - result.order_voucher[0].value);
            }
            }
            let shipping_address= {}
            result.order_billing.forEach(os=>{
                shipping_address.first_name=os.firstname,
                shipping_address.last_name=os.lastname,
                shipping_address.email=os.email,
                shipping_address.phone=os.phone_number,
                shipping_address.address = os.address,
                shipping_address.city = os.regency,
                shipping_address.postal_code = os.postcode
            });
            let customer_details = {
                first_name: result.order_billing[0].firstname,
                last_name:result.order_billing[0].lastname,
                email:result.order_billing[0].email,
                phone:result.order_billing[0].phone_number,
                shipping_address: shipping_address
            }
       
        
          
            let parameter={
                "transaction_details": {
                    "order_id":result.orders[0].id,
                    "gross_amount": total,
                }, 
                "enabled_payments": [req.body.payment_method],
                "customer_details": customer_details,
                "credit_card": {
                    "secure": true,
                    "save_card": true
                }
            }
            if(req.body.payment_method == "gopay"){
                parameter.gopay = {
                 "enable_callback": true,
                 "callback_url": "http://gopay.com"
                }
        
            }

            let snap = new midtransClient.Snap({
                isProduction: false,
                serverKey: keys.midtrans.serverKey,
                clientKey: keys.midtrans.clientKey
            });

            // let parameter = {
            //     "transaction_details": {
            //         "order_id": "test-transaction-52562141",
            //         "gross_amount": 200000
            //     },
            //     // "credit_card": {
            //     //     "secure": true
            //     // },
            //     "enabled_payments": [req.body.payment_method],
            //     // "gopay": {
            //     //     "enable_callback": true,
            //     //     "callback_url": "http://gopay.com"
            //     // }
            // };
            snap.createTransaction(parameter)
                .then((transaction) => {
         
                    // transaction token
                    let transactionToken = transaction.token;
                    return res.status(200).json(transactionToken)
                    // transaction redirect url
                    let transactionRedirectUrl = transaction.redirect_url;
                    // console.log('transactionRedirectUrl:', transactionRedirectUrl);
                })
                .catch((e) => {
                    let notification = {
                        error: true,
                        message: "There is an error !",
                        notification: true
                    }
                    return res.status(400).json({ notification: notification});
                });
        }
    })
    
    

    
}