import db from '../../config/conn';
import keys from '../../config/keys';
import jwt from 'jsonwebtoken';
import UAparser from 'ua-parser-js'
import async from 'async';
import { validationRegister, ValidationUpdateProfile, ValidationUpdateAddress, ValidationLogin} from './validation';
import bcrypt from 'bcryptjs';
import passport from 'passport';

export const loginUser = (req,res,next)=>{

    const { errors, isValid } = ValidationLogin(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }


    passport.authenticate('local', function (err, user, info) {

        if (err) { return next(err); }
        if (!user) { return res.status(400).json({message:info.message}) }
        req.login(user, function (err) {
            if (err) { return next(err); }
            if (user) {
                let payload = { user };
                let queryInsertUserSession = `INSERT INTO user_session (user_id,session_id) values (${user.id},'${req.sessionID}')`;
                db.query(queryInsertUserSession, (err, result) => {
                    if (err) return res.status(400).json(err);
                    if (result.affectedRows > 0) {
                        jwt.sign(
                            payload,
                            keys.jwt.secretOrPrivateKey,
                            {
                                expiresIn: keys.jwt.expiresIn
                            }, (err, token) => {
                                return res.status(200).json({ user: token });
                            });
                    }
                })
            }
        });
    
      


    })(req, res, next);
}

export const updateSession = (req,res)=>{
    
    if (req.cookies.hammerstout_ss){
        let oldSession = jwt.verify(req.cookies.hammerstout_ss, keys.jwt.secretOrPrivateKey);
    
    if(oldSession.session_id !== req.sessionID){
        let queryUpdated = `UPDATE carts set session_id ='${req.sessionID}' 
    where session_id = '${oldSession.session_id}' and id = ${oldSession.cart_id} `;
        db.query(queryUpdated, (error, result) => {
            if (error) return res.status(400).json(error);
            if (result.affectedRows > 0) {


                const oldSession = jwt.verify(req.cookies.hammerstout_ss, keys.jwt.secretOrPrivateKey);

                const payload = {
                    session_id: req.sessionID,
                    cart_id: oldSession.cart_id
                }
                const dataToken = jwt.sign(payload, keys.jwt.secretOrPrivateKey, { expiresIn: keys.jwt.expiresIn });
                res.cookie("hammerstout_ss", dataToken, { sameSite: true });


                return res.status(200).json({ error: false, message: "OK" });
            } else {
                return res.status(200).json({ error: false, message: "NONE" });
            }
        })
    }else{
        return res.status(200).json({ error: false, message: "NONE" });
    }
    }else{
        return res.status(200).json({ error: false, message: "NONE" });
    }

}

export const getUserInfo = (req,res)=>{
    if(req.user){

    let querySelectUserSession = `SELECT 
        u.id,
        u.displayName,
        u.email,
        u.gender,
        u.firstname,
        u.lastname,
        up.providerId,
        up.provider,
        ui.birthday,
        ui.phone_number from user as u 
    left join user_provider as up on u.id = up.user_id 
    left join user_session as us on u.id = us.user_id 
    left join user_information as ui on u.id = ui.user_id
    where us.session_id = '${req.sessionID}' `

    
        db.query(querySelectUserSession,(error,result)=>{
        if(error)return res.status(400).json({error:true,message:"ERROR FROM GET USER INFO"});
        if(result.length >0 ){
            let payload={user:result[0]};
            jwt.sign(
                payload,
                keys.jwt.secretOrPrivateKey,
                {
                    expiresIn: keys.jwt.expiresIn
                }, (err, token) => {
                    if(err) return res.status(400).json(err);
                    return res.status(200).json(token);
                    
                    // return res.redirect(keys.origin.url + "?token=" + token);
                });
           
        }
        if(result.length === 0){
            return res.status(400).json({error:true,message:"NO DETECTED USER",isAuth:false});
        }
    })
    }else{
        return res.status(400).json({ error: true, message: "NO DETECTED USER", isAuth: false });
    }
}

export const loginGoogleRedirect = (req,res)=>{

    let payload = {
        id: req.user.id,
        email: req.user.email,
        providerId: req.user.providerId,
        provider: req.user.provider
    };
 
    if (req.user.gender) payload.gender = req.user.gender;
    if (req.user.displayName) payload.displayName = req.user.displayName;
    if (req.user.lastname) payload.lastname = req.user.lastname;
    if (req.user.firstname) payload.firstname = req.user.firstname;
    if (req.user.birthday) payload.birthday = req.user.birthday;
    if (req.user.phone_number) payload.phone_number = req.user.phone_number;
    

    let queryInsertUserSession = `INSERT INTO user_session (user_id,session_id) values (${payload.id},'${req.sessionID}')`;
  
    db.query(queryInsertUserSession, (error, result) => {
        if (error) {
            return res.status(400).json(error);
        } 
        if(result){
            jwt.sign(
                payload,
                keys.jwt.secretOrPrivateKey,
                {
                    expiresIn: keys.jwt.expiresIn
                }, (err, token) => {
                    return res.redirect(keys.origin.url + "?token=" + token);
                });
        }
    })
 
}

export const loginFacebookRedirect =(req,res)=>{

    let payload = {
        id: req.user.id,
        email: req.user.email,
        providerId: req.user.providerId,
        provider: req.user.provider
    };
    if (req.user.gender) payload.gender = req.user.gender;
    if (req.user.displayName) payload.displayName = req.user.displayName;
    if (req.user.lastname) payload.lastname = req.user.lastname;
    if (req.user.firstname) payload.firstname = req.user.firstname;
    if(req.user.birthday) payload.birthday = req.user.birthday;
    if(req.phone_number) payload.phone_number = req.user.phone_number;
 
    let queryInsertUserSession = `INSERT INTO user_session (user_id,session_id) values (${payload.id},'${req.sessionID}')`;
    db.query(queryInsertUserSession, (err, result) => {
        if (err) return res.status(400).json(err);
        if (result.affectedRows > 0) {
            jwt.sign(
                payload,
                keys.jwt.secretOrPrivateKey,
                {
                    expiresIn: keys.jwt.expiresIn
                }, (err, token) => {
                    return res.redirect(keys.origin.url + "/redirect?token=" + token);
                });
        }
    })

}


export const registerUser = (req,res)=>{
   
    const { errors, isValid } = validationRegister(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }
    let queryFindUser = `SELECT email from user_account where email = ?;SELECT email from user where email = ? and is_provider = 1;`;
    db.query(queryFindUser, [req.body.email, req.body.email], (err, result) => {
       
        if (err) return res.status(400).json({error:true,message:"ERROR FROM REGISTER"});
        if (result[0].length > 0) {
            return res.status(400).json({ error: true, email: 'Email is already registered' });
        }
        if (result[1].length > 0) {
            return res.status(400).json({ error: true, email: 'Email is already registered using social media'});
        } 
    })
    let queryInsert = 'INSERT into user set ?; INSERT into user_account set user_id = (select u.id from user as u order by u.id desc limit 1), ?;';
    // let querySelectUser = `INSERT `
    let password = req.body.password;
    bcrypt.genSalt(10, (err, salt) => {
        //10 adalah berapa banyak karakter
        bcrypt.hash(req.body.password, salt, (err, hash) => {
            if (err) {
                throw err;
            }
            if(hash){
                db.query(queryInsert, [{ displayName: req.body.displayName, email: req.body.email }, { email: req.body.email, password: hash }], (err, result) => {
                    if (err) return res.status(400).json({ error: true, message: "error from register" });
                    if (result) {
                        return res.status(200).json({ error: false, message: "SUCCESS REGISTER" });
                    }
                })
            }
        
        });
    });

}

export const updateProfile =(req,res)=>{
  
    const { errors, isValid } = ValidationUpdateProfile(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }

    let queryUpdate = `UPDATE 
    user set ? 
    where id = (SELECT user_id from user_session where session_id = '${req.sessionID}'); 
    update  user_information set ? 
    where user_id = (SELECT user_id from user_session where session_id = '${req.sessionID}');`;

    let queryInsert = `UPDATE 
    user set ? 
    where id = (SELECT user_id from user_session where session_id = '${req.sessionID}'); 
    INSERT into user_information set ?, user_id = ${req.user.id};`;
    let querySelectUserSession = `SELECT 
        u.id,
        u.displayName,
        u.email,
        u.gender,
        u.firstname,
        u.lastname,
        up.providerId,
        up.provider,
        ui.birthday,
        ui.phone_number from user as u 
    left join user_provider as up on u.id = up.user_id 
    left join user_session as us on u.id = us.user_id 
    left join user_information as ui on u.id = ui.user_id
    where us.session_id = '${req.sessionID}' and us.user_id = ${req.user.id} `;

    let querySelectUserInformation = `
    SELECT * from user_information where user_id = ${req.user.id}`;
    if (req.body.user_information.birthday) req.body.user_information.birthday = new Date(req.body.user_information.birthday);
    async.parallel({
        checkInformation:function(callback){
            db.query(querySelectUserInformation,(err,result)=>{
                if(result.length > 0){
                    db.query(queryUpdate, [req.body.user, req.body.user_information],(err,result)=>{
                        callback(err,result);
                    })
                }
                if(result.length === 0){
                    db.query(queryInsert,[req.body.user,req.body.user_information],(err,result)=>{
                        callback(err, result);
                    })
                }
                if(err){
                    callback(err,null);
                }
            })
        }
    },function(err,result){
        if(err){
            return res.status(400).json({ error: true, message: "ERROR FROM GET USER"});
        }
        if(result){
            db.query(querySelectUserSession, (err, result) => {
                if(err){
                    return res.status(400).json({ error: true, message: "ERROR FROM GET USER", err: err });
                }
                if(result.length > 0){
                    let user = result[0];
                    let payload = { user };
                    let token_user = jwt.sign(payload, keys.jwt.secretOrPrivateKey, { expiresIn: keys.jwt.expiresIn });

                    return res.status(200).json({ user, token_user });
                }
                if(result.length ===0){
                    return res.status(400).json({ error: true, message: "ERROR FROM GET USER" });
                }
            })

        }
    })

  
}

export const updateAddress = (req,res)=>{
    let ua = UAparser(req.headers['user-agent']);
    let ip_address = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    if (ip_address.substr(0, 7) == "::ffff:") {
        ip_address = ip_address.substr(7)
    }
    if (ip_address == "::1") {
        ip_address = "127.0.0.1"
    }
    const { errors, isValid } = ValidationUpdateAddress(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }
    let data ={
        address: req.body.address,
        postcode: req.body.postcode,
        province_id: req.body.province_id.value,
        regency_id: req.body.regency_id.value,
        district_id: req.body.district_id.value,
        village_id: req.body.village_id.value
    }

    let queryFindAddressUser = `SELECT 
    ua.province_id,
    ua.regency_id,
    ua.district_id,
    ua.village_id,
    ua.address,
    ua.postcode,
    p.name as province_name,
    r.name as regency_name,
    d.name as district_name,
    v.name as village_name
    from user_address as ua 
    left join provinces as p on ua.province_id = p.id
    left join regencies as r on ua.regency_id = r.id
    left join districts as d on ua.district_id = d.id
    left join villages as v on ua.village_id = v.id
    where ua.user_id =(SELECT user_id from user_session where session_id = '${req.sessionID}') `;

    let queryFindAddress = `SELECT ua.id from user_address as ua where ua.user_id =(SELECT user_id from user_session where session_id = '${req.sessionID}') `;


    let queryUpdateAddress = `UPDATE user_address set ? where user_id = (SELECT user_id from user_session where session_id = '${req.sessionID}')`;
    let queryInsertAddress = `INSERT INTO user_address set user_id = (SELECT user_id from user_session where session_id = '${req.sessionID}'), ?`;

    let querySelectSession = `
    SELECT ss.id from session as ss
    LEFT JOIN session_browser as sb on ss.id = sb.session_id
    LEFT JOIN session_os as so on ss.id = sb.session_id
    LEFT JOIN session_engine as se on ss.id = se.session_id
    LEFT JOIN session_device as sd on ss.id = se.session_id
    where ss.ip_address = '${ip_address}' and ss.id = '${req.sessionID}' and sb.name = '${ua.browser.name}' and sb.version = '${ua.browser.version}' and so.name = '${ua.os.name}' and so.version = '${ua.os.version}' and se.name = '${ua.engine.name}' and se.version = '${ua.engine.version}' 
    ${ua.device.model ? ` and sd.model='${ua.device.model}'` : ''} 
    ${ua.device.type ? ` and sd.type='${ua.device.type}'` : ''} 
    ${ua.device.vendor ? ` and sd.vendor='${ua.device.vendor}'` : ''}
    group by ss.id,ss.ip_address`;
    let querySelectOnlySession = `SELECT ss.id from session as ss where ss.ip_address = '${ip_address}' and ss.id = '${req.sessionID}'  group by ss.id,ss.ip_address`;

    let querySelectUserSession = `SELECT u.id from user as u 
    left join user_provider as up on u.id = up.user_id 
    left join user_session as us on u.id = us.user_id 
    left join user_information as ui on u.id = ui.user_id
    where us.session_id = '${req.sessionID}' `

    let queryCheckSession = `${querySelectOnlySession}; ${querySelectSession};${querySelectUserSession};${queryFindAddress};`;

  
    db.query(queryCheckSession,(error,result)=>{
        if(error)return res.status(400).json({error:true,message:"ERROR FROM FIND ANDDRESS"});
        if(result[0].length > 0 && result[1].length > 0 && result[2].length > 0 && result[3].length > 0){
            db.query(queryUpdateAddress,[data],(err,result)=>{
                    if(err)return res.status(400).json({error:true,message:"ERROR FROM UPDATE ADDRESS"});
                    if (result){
                        db.query(queryFindAddressUser,(error,result)=>{
                            if (error) return res.status(400).json(error);
                            if(result.length > 0){
                                 let resultData =result[0];
                                let payload = { resultData};
                                let token_a = jwt.sign(payload,keys.jwt.secretOrPrivateKey,{expiresIn:keys.jwt.expiresIn});


                                let notification = {
                                    error: false,
                                    message: "ADDRESS HAS BEEN UPDATED",
                                    notification: true
                                }
                                return res.status(200).json({ address: resultData, token_a, notification: notification});
                            }
                        })
                    }
                })        
        }
        if (result[0].length > 0 && result[1].length > 0 && result[2].length > 0 && result[3].length == 0){
            db.query(queryInsertAddress,[data], (err, result) => {
                if (err) return res.status(400).json(err);
                if (result) {
                    db.query(queryFindAddressUser, (error, result) => {
                        if (error) return res.status(400).json({ error: true, message: "ERROR FROM FIND ADDRESS AFTER INSERTED" });
                        if(result.length > 0){
                            let resultData = result[0];
                            let payload = { resultData };
                            let token_a = jwt.sign(payload, keys.jwt.secretOrPrivateKey, { expiresIn: keys.jwt.expiresIn });

                            let notification ={
                                error:false,
                                message:"ADDRESS HAS BEEN CREATED",
                                notification: true
                            }
                            return res.status(200).json({ address: resultData, token_a, notification: notification });
                        }
                    })
                }
            }) 
        }
        if (result[0].length === 0 || result[1].length == 0 || result[2].length == 0){
            req.logout();
            req.session.destroy(function (err) {
                // cannot access session here
            })
            return res.status(400).json({error:true,message:"CANT FIND ANY USER "});
        }
    })
}

export const getUserAdddress =(req,res)=>{

    let queryFindAddressUser = `SELECT 
    ua.province_id,
    ua.regency_id,
    ua.district_id,
    ua.village_id,
    ua.address,
    ua.postcode,
    p.name as province_name,
    r.name as regency_name,
    d.name as district_name,
    v.name as village_name
    from user_address as ua 
    left join provinces as p on ua.province_id = p.id
    left join regencies as r on ua.regency_id = r.id
    left join districts as d on ua.district_id = d.id
    left join villages as v on ua.village_id = v.id
    where ua.user_id = ${req.user.id} `;



 

    // let querySelectSession = `
    // SELECT ss.id from session as ss
    // LEFT JOIN session_browser as sb on ss.id = sb.session_id
    // LEFT JOIN session_os as so on ss.id = sb.session_id
    // LEFT JOIN session_engine as se on ss.id = se.session_id
    // LEFT JOIN session_device as sd on ss.id = se.session_id
    // where ss.ip_address = '${ip_address}' and ss.id = '${req.sessionID}' and sb.name = '${ua.browser.name}' and sb.version = '${ua.browser.version}' and so.name = '${ua.os.name}' and so.version = '${ua.os.version}' and se.name = '${ua.engine.name}' and se.version = '${ua.engine.version}' 
    // ${ua.device.model ? ` and sd.model='${ua.device.model}'` : ''} 
    // ${ua.device.type ? ` and sd.type='${ua.device.type}'` : ''} 
    // ${ua.device.vendor ? ` and sd.vendor='${ua.device.vendor}'` : ''}
    // group by ss.id,ss.ip_address`;
    // let querySelectOnlySession = `SELECT ss.id from session as ss where ss.ip_address = '${ip_address}' and ss.id = '${req.sessionID}'  group by ss.id,ss.ip_address`;

    // let querySelectUserSession = `SELECT u.id from user as u 
    // left join user_provider as up on u.id = up.user_id 
    // left join user_session as us on u.id = us.user_id 
    // left join user_information as ui on u.id = ui.user_id
    // where us.session_id = '${req.sessionID}' `

    // let queryCheckSession = `${querySelectOnlySession}; ${querySelectSession};${querySelectUserSession};${queryFindAddressUser};`;

    db.query(queryFindAddressUser,(err,result)=>{
        if(err){
            req.logout();
            req.session.destroy(function (err) {
                // cannot access session here
            })
            return res.status(400).json({ error: true, message: "CANT FIND ANY USER " });
        }
        if(result.length > 0){
           let resultData = result[0];
            let payload = { resultData };
            let token_a = jwt.sign(payload, keys.jwt.secretOrPrivateKey, { expiresIn: keys.jwt.expiresIn });
            return res.status(200).json({ address: resultData, token_a });
        }
        if(result.length === 0){
            return res.status(400).json({ error: false, message: "CANT FIND ANY USER ADDRESS" });
        }
    })
   
}
export const logout = (req,res)=>{
    req.logout();
    res.clearCookie("hammerstout_ss");
    req.session.destroy(function (err) {
        // cannot access session here
    })
    return res.status(200).json("LOGOUT");
}