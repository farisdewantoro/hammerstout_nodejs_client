import db from '../../config/conn';
import UAparser from 'ua-parser-js'
import async from 'async';
import jwt from 'jsonwebtoken';
import keys from '../../config/keys';


export const cartTrack = (req,res)=>{
    let queryFindCartList = `select 
    ci.id as cart_items_id,
    p.name as product_name,
    p.slug as product_slug,
    p.description,
    p.regular_price,
    c.name as category_name,
    c.slug,
    ct.name as category_type,
    ct.slug as category_type_slug,
    p.id as product_id,
    pa.id as product_attribute_id,
    pv.id as product_variant_id,
    pd.discount_percentage,
    pd.discount_value,
    i.link,i.caption,
    i.alt,
    pa.size,
    pa.stock,
    crt.active as cart_status,
    ci.quantity from products as p 
left join product_category as pc on p.id = pc.product_id 
left join categories as c on pc.category_id = c.id 
left join product_variant as pv on p.id = pv.product_id
left join category_type as ct on pv.category_type_id = ct.id
left join product_discount as pd on pd.id = 
(SELECT pd1.id from product_discount as pd1 where p.id = pd1.product_id and now() between pd1.valid_from and pd1.valid_until)
left join product_image as pi on pi.id = (SELECT pi1.id from product_image as pi1 where pi1.product_id = p.id order by pi1.product_id asc limit 1)
left join images as i on pi.image_id = i.id 
left join product_attribute as pa on p.id = pa.product_id and pv.id = pa.product_variant_id
left join cart_items as ci on pv.id = ci.product_variant_id and p.id = ci.product_id and pa.id = ci.product_attribute_id
left join carts as crt on ci.cart_id = (SELECT crt1.id from carts as crt1 where crt1.session_id = ?
) where crt.session_id = ? and ci.quantity <= pa.stock

`;

 
    if(req.session.visitor ){
        let visitor = req.session.visitor;
    
        db.query(queryFindCartList, [visitor.session_id, visitor.session_id], (error, result) => {
            
            if (result) {
                let token_cart = {
                    result
                }
                
                let token_c = jwt.sign(token_cart, keys.jwt.secretOrPrivateKey, { expiresIn: keys.jwt.expiresIn });
                return res.status(200).json({ cart_list: result, error: false, message: "OK", token_c });
            } 
            if (error) {
                return res.status(400).json(error);
            }

        })
    }else{
        return res.status(400).json("ERROR");
    }


  
 
}

// export const loginGoogleRedirect = (req, res) => {
//     return res.status(200).json(req.user);
// }

