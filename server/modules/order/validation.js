import Validator from 'validator';
import isEmpty from '../../../validations/is-empty';

export const validationSubmitOrder = (data) => {
    let errors = {
        carts:{},
        shipping:{},
        user:{},
        vouchers:{}
    }
    if (!data.carts instanceof Array || data.carts.length === 0  ){
        errors.carts.error = 'MUST BE PROVIDE';
    }
    if(data.carts.length > 0){
        data.carts.forEach(c => {
            if (isEmpty(c.product_id)) {
                errors.carts.product_id = 'MUST BE PROVIDE';
            }
            if (isEmpty(c.product_variant_id)) {
                errors.carts.product_variant_id = 'MUST BE PROVIDE';
            }
            if (isEmpty(c.cart_items_id)) {
                errors.carts.cart_items_id = 'MUST BE PROVIDE';
            }
            if (isEmpty(c.product_attribute_id)) {
                errors.carts.product_attribute_id = 'MUST BE PROVIDE';
            }
        });
    }

    if(Object.keys(data.vouchers).length > 0){
            if(isEmpty(data.vouchers.description)){
                errors.vouchers.description = 'MUST BE PROVIDE';
            }
        if (isEmpty(data.vouchers.id)) {
            errors.vouchers.id = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.vouchers.name)) {
            errors.vouchers.name = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.vouchers.value)) {
            errors.vouchers.value = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.vouchers.voucher_type_id)) {
            errors.vouchers.voucher_type_id = 'MUST BE PROVIDE';
        }
    }


    if (Object.keys(data.shippingSelected).length === 0){
        errors.shipping = 'MUST BE PROVIDE';
    }

    if(Object.keys(data.shippingSelected).length > 0){
        if (isEmpty(data.shippingSelected.name)){
            errors.shipping.name = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.shippingSelected.service)) {
            errors.shipping.service = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.shippingSelected.description)) {
            errors.shipping.description = 'MUST BE PROVIDE';
        }
        if(typeof data.shippingSelected.cost == "undefined" || data.shippingSelected.cost == '' ){
            errors.shipping.cost = 'MUST BE PROVIDE';
        }
        if (data.shippingSelected.cost instanceof Array && data.shippingSelected.cost.length === 0){
            errors.shipping.cost = 'MUST BE PROVIDE';
        }
        if (data.shippingSelected.cost instanceof Array && data.shippingSelected.cost.length > 0){
            if (isEmpty(data.shippingSelected.cost[0].value)){
                errors.shipping.cost = 'MUST BE PROVIDE';
            }
            if(isEmpty(data.shippingSelected.cost[0].etd)){
                errors.shipping.cost = 'MUST BE PROVIDE';
            }
        }
    }

    console.log(Object.keys(data.shippingSelected).length);
    if (Object.keys(data.user).length === 0) {
        errors.user = 'MUST BE PROVIDE';
    }


  
    if (Object.keys(data.user).length > 0){
        if (isEmpty(data.user.address)) {
            errors.user.address = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.user.email)) {
            errors.user.email = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.user.phone_number)) {
            errors.user.phone_number = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.user.firstname)) {
            errors.user.firstname = 'MUST BE PROVIDE';
        }
        if (isEmpty(data.user.lastname)) {
            errors.user.lastname = 'MUST BE PROVIDE';
        }

        if (isEmpty(data.user.postcode)) {
            errors.user.postcode = 'MUST BE PROVIDE';
        }
        if (typeof data.user.district_id == "undefined" || data.user.district_id == "" || data.user.district_id == null ) {
            errors.user.district_id = 'MUST BE PROVIDE';
        }

        if (typeof data.user.province_id == "undefined" || data.user.province_id == "" || data.user.province_id == null) {
            errors.user.province_id = 'MUST BE PROVIDE';
        }
        if (typeof data.user.regency_id == "undefined" || data.user.regency_id == "" || data.user.regency_id == null) {
            errors.user.regency_id = 'MUST BE PROVIDE';
        }
        if (typeof data.user.village_id == "undefined" || data.user.village_id == "" || data.user.village_id == null) {
            errors.user.village_id = 'MUST BE PROVIDE';
        }

        if (data.user.district_id && !data.user.district_id.hasOwnProperty('label')){
            errors.user.district_id = 'MUST BE PROVIDE';
        }
        if (data.user.province_id && !data.user.province_id.hasOwnProperty('label')){
            errors.user.province_id = 'MUST BE PROVIDE';
        }
        if (data.user.regency_id && !data.user.regency_id.hasOwnProperty('label')){
            errors.user.regency_id = 'MUST BE PROVIDE';
        }
        if (data.user.village_id && !data.user.village_id.hasOwnProperty('label')){
            errors.user.village_id = 'MUST BE PROVIDE';
        }

    }


   
  


  
    if (Object.keys(errors.carts).length === 0){
        delete errors.carts;
    }
 

    if(Object.keys(errors.user).length == 0){
        delete errors.user;
    }
    
    if(Object.keys(errors.shipping).length == 0){
        delete errors.shipping;
    }
    if (Object.keys(errors.vouchers).length == 0) {
        delete errors.vouchers;
    }


    return {
        errors,
        isValid: isEmpty(errors)
    }
}
