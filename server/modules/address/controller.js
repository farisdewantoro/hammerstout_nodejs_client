import db from '../../config/conn';
import keys from '../../config/keys';
import jwt from 'jsonwebtoken';

export const getProvinces =(req,res)=>{
    let queryGetAllProvinces = `SELECT * FROM provinces`;
    db.query(queryGetAllProvinces,(err,result)=>{
        if(err)return res.status(400).json(err);
        if(result.length > 0){
            return res.status(200).json(result);
        }
    })
}

export const findCity =(req,res)=>{
    let queryFindRegency = `SELECT * from regencies where province_id = '${req.body.value}' `;
    db.query(queryFindRegency,(err,result)=>{
        if(err)return res.status(400).json(err);
        if(result.length > 0){
            return res.status(200).json(result);
        }
    })
}
export const findDistricts = (req, res) => {
    let queryFindDistricts = `SELECT * from districts where regency_id = '${req.body.value}' `;
    db.query(queryFindDistricts, (err, result) => {
        if (err) return res.status(400).json(err);
        if (result.length > 0) {
            return res.status(200).json(result);
        }
    })
}
export const findVillages = (req, res) => {
    let queryfindVillages = `SELECT * from villages where district_id = '${req.body.value}' `;
    db.query(queryfindVillages, (err, result) => {
        if (err) return res.status(400).json(err);
        if (result.length > 0) {
            return res.status(200).json(result);
        }
    })
}
